// Pre_Processing.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Header.h"

int main(int argc, char** argv)
{
    //string imagesDir("D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseAtual/Gallery/");
    string imagesDir("D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseAtual/Probe/");

    vector<string> imagesNames;

    if (argc > 1)
    {
        imagesDir = argv[1];
        imagesNames.clear();
        for (int i = 2; i < argc; ++i)
        {
            imagesNames.push_back(argv[i]);
        }
    }

    if (imagesNames.empty())
    {
        for (std::filesystem::directory_entry entry : std::filesystem::directory_iterator(imagesDir)) {
            if (!entry.is_directory()) imagesNames.push_back(entry.path().filename().string());
        }
    }

    for (int figure = 0; figure < imagesNames.size(); ++figure) {

        Mat image = imread(imagesDir + imagesNames[figure], IMREAD_GRAYSCALE); // Read the file

        if (image.empty())  // Check for invalid input
        {
            cout << "Nao foi possivel abrir ou encontrar a impressao digital: " << imagesNames[figure] << std::endl;
            continue;
        }

        Mat imgenh, imorient, imorientshow, imsegmented, imwave, imwave8u, imfilt, imthin, lgpfeatures;
        float meanwavelen = 0;
        int blocksize = 20;

        cout << "Impressao digital: " << imagesNames[figure] << "\n";
        cout << "Processando...\n";

        normaliseMeanStd(image, image, 0.5, 0.5);
        imageEnhance_v3(image, imgenh);
        segment(imgenh, imsegmented);

        orientImage(imgenh, imsegmented, imorient, imorientshow, blocksize);

        enhanceOrientImageV2(imorient, imsegmented, imorient, imorientshow, blocksize);
        enhanceOrientImage(imorient, imsegmented, imorient, imorientshow, blocksize);
        roundToMult5(imorient, imorient);

        wavelengthImage(imgenh, imorient, imwave, blocksize);
        gaborFiltImage(imgenh, imorient, imwave, imfilt, blocksize, meanwavelen);

        normFloatandConvert8U(imwave, imwave8u);

        calcLGPFeatures(image, imsegmented, lgpfeatures, imagesDir, imagesNames[figure]);

        thinning(imfilt, imthin, ximgproc::THINNING_ZHANGSUEN);

        writeDirMap(imorient, blocksize, imagesDir, imagesNames[figure]);
        writeFreqMap(imwave8u, blocksize, imagesDir, imagesNames[figure]);
        imwrite(imagesDir + "Filtradas/imfilt_" + imagesNames[figure], imfilt);
        imwrite(imagesDir + "Filtradas/imenh_" + imagesNames[figure], imgenh);
        imwrite(imagesDir + "Filtradas/imorientshow_" + imagesNames[figure], imorientshow);
        imwrite(imagesDir + "Filtradas/imsegmented_" + imagesNames[figure], imsegmented);
        imwrite(imagesDir + "Filtradas/imorient_" + imagesNames[figure], imorient);
        imwrite(imagesDir + "Filtradas/imwave_" + imagesNames[figure], imwave8u);
        imwrite(imagesDir + "Filtradas/imthin_" + imagesNames[figure], imthin);

        cout << "Processamento concluido!\n";
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
