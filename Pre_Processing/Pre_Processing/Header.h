#pragma once
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/stereo.hpp>
#include <opencv2/imgcodecs.hpp>

#include "peaks.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <filesystem>
#include <numeric>

using namespace std;
using namespace cv;

void imageEnhance_v1(InputArray image, OutputArray imgenhanced);
void imageEnhance_v2(InputArray image, OutputArray imgenhanced);
void imageEnhance_v3(InputArray image, OutputArray imgenhanced);
void findMinutiae(InputArray image, InputArray imthin, InputArray imorient, InputArray imsegmented, OutputArray mapmin, string dirimages, string fpname, float stddevimthreshold, int blocksize, float meanwavelength, int fpclassification);
int position(InputArray image, int x, int y, int current);
int angleposition(InputArray imorient, int x, int y, int current, int blocksize);
void normaliseMeanStd(InputArray image, OutputArray imgnorm, float reqmean, float reqvar);
void stdDevImage(InputArray image, OutputArray mask, int blocksize, float& stddevfullimg);
void normFloatandConvert8U(InputArray imageFloat, OutputArray image8u);
void imadjust(const Mat1b& src, Mat1b& dst, int tol = 1, Vec2i in = Vec2i(0, 255), Vec2i out = Vec2i(0, 255));
void freqFilter(InputArray image, OutputArray imgenhanced, int blocksize);
void orientImage(InputArray image, InputArray imsegmented, OutputArray orientim, OutputArray orientimshow, int blocksize);
void wavelengthImage(InputArray image, InputArray imorient, OutputArray wavelengthim, int blocksize);
void gaborFiltImage(InputArray image, InputArray imorient, InputArray imwave, OutputArray imfilt, int blocksize, float& meanwavelen);
void enhanceOrientImage(InputArray orientim, InputArray imsegmented, OutputArray orientimenh, OutputArray orientimenhshow, int blocksize);
void enhanceOrientImageV2(InputArray orientim, InputArray imsegmented,OutputArray orientimenh, OutputArray orientimenhshow, int blocksize);
string removeExtension(string filename);
void segment(InputArray image, OutputArray segmented);
float meanWavelength(InputArray imwave, int blocksize);
void calcLGPFeatures(InputArray image, InputArray imsegmented, OutputArray lgp, string dirimages, string filename);
void readFPclassification(string dir, vector<string>& file, vector<int>& classification);
void roundToMult5(InputArray dirmap, OutputArray dirmaproundout);
void writeDirMap(InputArray imorient, int blocksize, string dirimages, string filename);
void writeFreqMap(InputArray imwave, int blocksize, string dirimages, string filename);

void writeFreqMap(InputArray imwave, int blocksize, string dirimages, string filename)
{
    ofstream freqmapfile;
    Mat waveim;
    int addy, addx;

    imwave.copyTo(waveim);

    //pad image to blocksize
    addy = waveim.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = waveim.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(waveim, waveim, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(255));

    freqmapfile.open(dirimages + "Filtradas/" + removeExtension(filename) + ".freq");

    for (int y = 0; y < waveim.rows; y += blocksize) {
        for (int x = 0; x < waveim.cols; x += blocksize) {
            freqmapfile << y / blocksize << " , " << x / blocksize << " : " << (int)waveim.at<uchar>(y, x) << endl;
        }
    }

    freqmapfile.close();
}

void writeDirMap(InputArray imorient, int blocksize, string dirimages, string filename)
{
    ofstream dirmapfile;
    Mat orientim;
    int addy, addx;

    imorient.copyTo(orientim);

    //pad image to blocksize
    addy = orientim.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = orientim.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(orientim, orientim, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(255));

    dirmapfile.open(dirimages + "Filtradas/" + removeExtension(filename) + ".dir");

    for (int y = 0; y < orientim.rows; y += blocksize) {
        for (int x = 0; x < orientim.cols; x += blocksize) {
            dirmapfile << y / blocksize << " , " << x / blocksize << " : " << (int)orientim.at<uchar>(y, x) << endl;
        }
    }

    dirmapfile.close();
}

void roundToMult5(InputArray dirmap, OutputArray dirmaproundout)
{
    Mat dirmapround, dirmaproundaux, dirmaproundfloat;

    dirmap.copyTo(dirmapround);
    dirmapround.convertTo(dirmaproundfloat, CV_32FC1);

    int mt = 0;
    float rem = 0;

    for (int i = 0; i < dirmaproundfloat.rows; ++i) {
        for (int j = 0; j < dirmaproundfloat.cols; ++j) {
            if (dirmaproundfloat.at<float>(i, j) < 180) {
                mt = floor(dirmaproundfloat.at<float>(i, j) / 5.0f);
                rem = ((dirmaproundfloat.at<float>(i, j) / 5.0f) - mt);
                if (rem <= 0.5f) {
                    if ( (mt * 5) == 180 ) {
                        dirmaproundfloat.at<float>(i, j) = 0;
                    }
                    else {
                        dirmaproundfloat.at<float>(i, j) = mt * 5;
                    }
                }
                else {
                    if ( (mt * 5 + 5) == 180 ) {
                        dirmaproundfloat.at<float>(i, j) = 0;
                    }
                    else {
                        dirmaproundfloat.at<float>(i, j) = mt * 5 + 5;
                    }
                }
            }
        }
    }

    dirmaproundfloat.convertTo(dirmaproundaux, CV_8UC1);

    dirmaproundaux.copyTo(dirmaproundout);
}

void readFPclassification(string dir, vector<string>& file, vector<int>& classification)
{
    string filename(dir + "fpclassification.csv");

    ifstream fileclassifications;

    string delimiter = ",";
    string line, token;
    size_t pos = 0;

    fileclassifications.open(filename);

    if (fileclassifications.is_open()) {
        while (getline(fileclassifications, line)) {
            pos = line.find(delimiter);
            token = line.substr(0, pos);

            file.push_back(token);
            line.erase(0, pos + delimiter.length());
            classification.push_back(stoi(line));
        }

        fileclassifications.close();
    }
    else {
        cout << "Arquivo com as classificacoes das impressoes digitais nao encontrado!\n";
    }

    return;
}

void calcLGPFeatures(InputArray image, InputArray imsegmented, OutputArray lgp, string dirimages, string filename)
{
    float avglgp, scalefactor = 0.3;
    Mat imagein, segmented, imageinaux, segmentedaux, aux(Size(3, 3), CV_32F), lgpfeatures, histlgp;
    vector<int> pat8(8, 0), s(8, 0);
    const int histSize = 256;
    float range[2] = { 0, 256 };
    const float* histRange = { range };
    ofstream lgpfile;

    image.copyTo(imageinaux);
    imsegmented.copyTo(segmentedaux);

    resize(imageinaux, imagein, Size(round(image.cols() * scalefactor), round(image.rows() * scalefactor)), 0, 0, INTER_AREA);
    resize(segmentedaux, segmented, Size(round(image.cols() * scalefactor), round(image.rows() * scalefactor)), 0, 0, INTER_AREA);
    
    lgpfeatures = Mat::zeros(Size(round(image.cols() * scalefactor) - 2, round(image.rows() * scalefactor) - 2), CV_32FC1);

    lgpfile.open(dirimages + "Filtradas/" + removeExtension(filename) + ".lgp");

    for (int y = 1; y < imagein.rows - 1; ++y) {
        for (int x = 1; x < imagein.cols - 1; ++x) {
            if (((int)segmented.at<uchar>(y - 1, x - 1) < 10) && ((int)segmented.at<uchar>(y - 1, x) < 10) && ((int)segmented.at<uchar>(y - 1, x + 1) < 10) && ((int)segmented.at<uchar>(y, x - 1) < 10) && ((int)segmented.at<uchar>(y, x + 1) < 10) && ((int)segmented.at<uchar>(y + 1, x - 1) < 10) && ((int)segmented.at<uchar>(y + 1, x) < 10) && ((int)segmented.at<uchar>(y + 1, x + 1) < 10)) { //TODO: s� fazer para pixel que a regi�o esta dentro da de interesse
                int i = 0, j = 0;

                for (int a = y - 1; a < y + 2; ++a) {
                    for (int b = x - 1; b < x + 2; ++b) {
                        aux.at<float>(i, j) = (int)imagein.at<uchar>(a, b);

                        j += 1;
                    }

                    j = 0;
                    i += 1;
                }

                aux = abs(aux - aux.at<float>(1, 1));

                pat8[0] = (int)aux.at<float>(0, 0);
                pat8[1] = (int)aux.at<float>(0, 1);
                pat8[2] = (int)aux.at<float>(0, 2);
                pat8[3] = (int)aux.at<float>(1, 2);
                pat8[4] = (int)aux.at<float>(2, 2);
                pat8[5] = (int)aux.at<float>(2, 1);
                pat8[6] = (int)aux.at<float>(2, 0);
                pat8[7] = (int)aux.at<float>(1, 0);

                avglgp = accumulate(pat8.begin(), pat8.end(), 0) / pat8.size();

                for (int a = 0; a < pat8.size(); ++a) {
                    pat8[a] = pat8[a] - avglgp;
                    if (pat8[a] >= 0) {
                        s[a] = 1;
                    }
                    else {
                        s[a] = 0;
                    }
                }

                lgpfeatures.at<float>(y - 1, x - 1) = s[0] * 128 + s[1] * 64 + s[2] * 32 + s[3] * 16 + s[4] * 8 + s[5] * 4 + s[6] * 2 + s[7] * 1;
            }
            else {
                lgpfeatures.at<float>(y - 1, x - 1) = -1;
            }
        }
    }

    calcHist(&lgpfeatures, 1, 0, Mat(), histlgp, 1, &histSize, &histRange);

    for (int i = 0; i < histlgp.rows; ++i) {
        if (i < histlgp.rows - 1) {
            lgpfile << histlgp.at<float>(i, 0) << ",";
        }
        else {
            lgpfile << histlgp.at<float>(i, 0);
        }
    }

    lgpfile.close();
    histlgp.copyTo(lgp);

    return;
}

string removeExtension(string filename)
{
    size_t pos = 0;
    string delimiter = ".";

    pos = filename.find(delimiter);
    filename.erase(pos, filename.length());

    return filename;
}

void enhanceOrientImageV2(InputArray orientim, InputArray imsegmented, OutputArray orientimenh, OutputArray orientimenhshow, int blocksize)
{
    Mat imorient, imsegment,line_rot_mat, line_mat, line_mat_rotated, imorientshow, imorient_temp, imorientshow_temp;
    int addy, addx, currentangle, previousangle, topangle, bottomangle, fowardangle, anglemean, angle, angleshow;
    Point2f line_mat_center(blocksize / 2.0f, blocksize / 2.0f);

    line_mat = Mat::zeros(Size(blocksize, blocksize), CV_8UC1);

    for (int j = 0; j < line_mat.cols; ++j) {
        line_mat.at<uchar>(blocksize / 2, j) = 255;
    }

    orientim.copyTo(imorient);
    imsegmented.copyTo(imsegment);

    //pad image to blocksize
    addy = imorient.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = imorient.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(imorient, imorient, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(0));
    copyMakeBorder(imsegment, imsegment, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(255));

    imorientshow = Mat::zeros(Size(imorient.cols, imorient.rows), CV_8UC1);

    for (int y = blocksize; y < imorient.rows - blocksize; y += blocksize) {
        for (int x = blocksize; x < imorient.cols - blocksize; x += blocksize) {
            currentangle = (int)imorient.at<uchar>(y, x);
            topangle = (int)imorient.at<uchar>(y - blocksize, x);
            previousangle = (int)imorient.at<uchar>(y, x - blocksize);
            bottomangle = (int)imorient.at<uchar>(y + blocksize, x);
            fowardangle = (int)imorient.at<uchar>(y, x + blocksize);
            anglemean = 255;

            if ((currentangle > 200) && ((int)imsegment.at<uchar>(y, x) < 10)) {
                if (topangle < 200 && previousangle < 200) {
                    if (abs(topangle - previousangle) < 80) {
                        anglemean = (topangle + previousangle) / 2;
                    }
                    else {
                        anglemean = topangle;
                    }
                }
                else if (topangle < 200 && fowardangle < 200) {
                    if (abs(topangle - fowardangle) < 80) {
                        anglemean = (topangle + fowardangle) / 2;
                    }
                    else {
                        anglemean = topangle;
                    }
                }
                else if (fowardangle < 200 && bottomangle < 200) {
                    if (abs(fowardangle - bottomangle) < 80) {
                        anglemean = (fowardangle + bottomangle) / 2;
                    }
                    else {
                        anglemean = fowardangle;
                    }
                }
                else if (previousangle < 200 && bottomangle < 200) {
                    if (abs(previousangle - bottomangle) < 80) {
                        anglemean = (previousangle + bottomangle) / 2;
                    }
                    else {
                        anglemean = previousangle;
                    }
                }
                else {
                    if (topangle < 200) {
                        anglemean = topangle;
                    }
                    else if (previousangle < 200) {
                        anglemean = previousangle;
                    }
                    else if (bottomangle < 200) {
                        anglemean = bottomangle;
                    }
                    else if (fowardangle < 200) {
                        anglemean = fowardangle;
                    }
                }
                for (int i = 0; i < blocksize; ++i) {
                    for (int j = 0; j < blocksize; ++j) {
                        imorient.at<uchar>(y + i, x + j) = (uchar)anglemean;
                    }
                }
            }
        }
    }

    for (int y = 0; y < imorient.rows; y += blocksize) {
        for (int x = 0; x < imorient.cols; x += blocksize) {
            //construct lines in image
            angle = (int)imorient.at<uchar>(y, x);
            angleshow = 90 - angle;
            line_rot_mat = getRotationMatrix2D(line_mat_center, angleshow, 1.0);
            warpAffine(line_mat, line_mat_rotated, line_rot_mat, line_mat.size());

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    imorientshow.at<uchar>(y + i, x + j) = line_mat_rotated.at<uchar>(i, j);
                }
            }
        }
    }

    imorient_temp = imorient(Rect(0, 0, imorient.cols - addx, imorient.rows - addy));

    imorientshow_temp = imorientshow(Rect(0, 0, imorientshow.cols - addx, imorientshow.rows - addy));

    imorient_temp.copyTo(orientimenh);

    imorientshow_temp.copyTo(orientimenhshow);
}

void enhanceOrientImage(InputArray orientim, InputArray imsegmented, OutputArray orientimenh, OutputArray orientimenhshow, int blocksize)
{
    Mat imorient, imsegment, line_rot_mat, line_mat, line_mat_rotated, imorientshow, imorient_temp, imorientshow_temp;
    int addy, addx, currentangle, previousangle, topangle, bottomangle, fowardangle, anglemean, angle, angleshow;
    Point2f line_mat_center(blocksize / 2.0f, blocksize / 2.0f);

    line_mat = Mat::zeros(Size(blocksize, blocksize), CV_8UC1);

    for (int j = 0; j < line_mat.cols; ++j) {
        line_mat.at<uchar>(blocksize / 2, j) = 255;
    }

    orientim.copyTo(imorient);
    imsegmented.copyTo(imsegment);

    //pad image to blocksize
    addy = imorient.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = imorient.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(imorient, imorient, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(0));
    copyMakeBorder(imsegment, imsegment, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(255));

    imorientshow = Mat::zeros(Size(imorient.cols, imorient.rows), CV_8UC1);

    for (int y = blocksize; y < imorient.rows - blocksize; y += blocksize) {
        for (int x = blocksize; x < imorient.cols - blocksize; x += blocksize) {
            if ( ((int)imsegment.at<uchar>(y, x) < 10) ) {
                currentangle = (int)imorient.at<uchar>(y, x);
                topangle = (int)imorient.at<uchar>(y - blocksize, x);
                previousangle = (int)imorient.at<uchar>(y, x - blocksize);
                bottomangle = (int)imorient.at<uchar>(y + blocksize, x);

                if ( (currentangle < 180) && (topangle < 180) && (previousangle < 180) && (bottomangle < 180) ) {
                    if (y == blocksize && (topangle < 10 || topangle > 170)) continue;
                    if (y == imorient.cols - 2 * blocksize && (bottomangle < 10 || bottomangle > 170)) continue;
                    if (topangle == 0 && bottomangle > 120) {
                        topangle = 180;
                    }
                    if (bottomangle == 0 && topangle > 120) {
                        bottomangle = 180;
                    }
                    if (abs(currentangle - topangle) > 20) {
                        if (abs(currentangle - bottomangle) > 30) {
                            if ((topangle < 90 && bottomangle > 90)) {
                                bottomangle += 180;
                            }
                            if ((topangle > 90 && bottomangle < 90)) {
                                topangle += 180;
                            }
                            anglemean = (topangle + bottomangle) / 2;
                            for (int i = 0; i < blocksize; ++i) {
                                for (int j = 0; j < blocksize; ++j) {
                                    imorient.at<uchar>(y + i, x + j) = (uchar)anglemean;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    for (int y = 0; y < imorient.rows; y += blocksize) {
        for (int x = 0; x < imorient.cols; x += blocksize) {
            //construct lines in image
            angle = (int)imorient.at<uchar>(y, x);
            angleshow = 90 - angle;
            line_rot_mat = getRotationMatrix2D(line_mat_center, angleshow, 1.0);
            warpAffine(line_mat, line_mat_rotated, line_rot_mat, line_mat.size());

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    imorientshow.at<uchar>(y + i, x + j) = line_mat_rotated.at<uchar>(i, j);
                }
            }
        }
    }

    imorient_temp = imorient(Rect(0, 0, imorient.cols - addx, imorient.rows - addy));

    imorientshow_temp = imorientshow(Rect(0, 0, imorientshow.cols - addx, imorientshow.rows - addy));

    imorient_temp.copyTo(orientimenh);

    imorientshow_temp.copyTo(orientimenhshow);
}

void segment(InputArray image, OutputArray segmented)
{
    Mat imagein;
    image.copyTo(imagein);

    medianBlur(imagein, imagein, 5);

    Mat mask = getStructuringElement(MORPH_CROSS, Size(3, 3));

    //adaptiveThreshold(imagein, imagein, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, 2);
    threshold(imagein, imagein, 180, 255, THRESH_BINARY);

    Mat seg;

    morphologyEx(imagein, seg, MORPH_OPEN, mask, Point(-1, -1), 10);
    morphologyEx(seg, seg, MORPH_DILATE, mask, Point(-1, -1), 10);

    seg.copyTo(segmented);
}

void gaborFiltImage(InputArray image, InputArray imorient, InputArray imwave, OutputArray imfilt, int blocksize, float& meanwavelen)
{
    Mat imagein, waveimage, angles, block, block_filtred, gabor_filter, img_filtred, imfilt_temp;
    Mat1b imgnegative, img_out;
    int addy, addx, filtersize;
    float defaultwavelength, sigma;

    image.copyTo(imagein);
    imwave.copyTo(waveimage);
    imorient.copyTo(angles);

    //make negative image ( imadjust(imgc,[0 1], [1 0]) )
    imadjust(imagein, imgnegative, 0, Vec2i(0, 255), Vec2i(255, 0));

    //pad image to blocksize
    addy = imagein.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = imagein.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(imgnegative, imgnegative, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(0));

    defaultwavelength = meanWavelength(waveimage, blocksize);

    img_filtred = Mat::zeros(Size(imgnegative.cols, imgnegative.rows), CV_8UC1);

    for (int y = 0; y < imgnegative.rows; y += blocksize) {
        for (int x = 0; x < imgnegative.cols; x += blocksize) {
            block = imgnegative(Rect(x, y, blocksize, blocksize));

            sigma = defaultwavelength * 0.5f;

            filtersize = round(3 * sigma);

            gabor_filter = getGaborKernel(Size(filtersize, filtersize), sigma, ((int)angles.at<uchar>(y, x) * CV_PI / 180), defaultwavelength, 1, 0); //CV_64F

            filter2D(block, block_filtred, -1, gabor_filter, Point(-1, -1), 0, BORDER_DEFAULT);

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    img_filtred.at<uchar>(y + i, x + j) = block_filtred.at<uchar>(i, j);
                }
            }
        }
    }

    meanwavelen = defaultwavelength;

    imfilt_temp = img_filtred(Rect(0, 0, img_filtred.cols - addx, img_filtred.rows - addy));

    //imadjust(imfilt_temp, img_out, 0, Vec2i(0, 255), Vec2i(255, 0)); //extract from valleys
    imfilt_temp.copyTo(img_out); //extract from ridge

    img_out.copyTo(imfilt);

}

float meanWavelength(InputArray imwave, int blocksize)
{
    Mat imagewave;
    vector<float> wavelengths;
    float accum, mean;

    imwave.copyTo(imagewave);
    accum = 0;
    mean = 0;

    for (int y = 0; y < imagewave.rows; y += blocksize) {
        for (int x = 0; x < imagewave.cols; x += blocksize) {
            if (imagewave.at<float>(y, x) > 0) {
                wavelengths.push_back(imagewave.at<float>(y, x));
            }
        }
    }

    for (int i = 0; i < wavelengths.size(); ++i) {
        accum = accum + wavelengths[i];
    }

    if (wavelengths.size() > 0) {
        mean = accum / wavelengths.size();
    }
    else {
        mean = 10; //default wavelength
    }

    return mean;
}

void wavelengthImage(InputArray image, InputArray imorient, OutputArray wavelengthim, int blocksize)
{
    Mat imagein, angles, window, window_rotated, window_rot_mat, wavelength_mat, wavelength_mat_temp;
    Mat1b imgnegative;
    int addy, addx, noofpeaks;
    float wavelength;
    int win_width = blocksize * 2;
    int win_height = blocksize;
    Point2f window_center(win_width / 2.0f, win_height / 2.0f);
    vector<float> xsignature;
    vector<int> peaks;

    xsignature.assign(win_width, 0);
    noofpeaks = 0;
    wavelength = 0;

    image.copyTo(imagein);
    imorient.copyTo(angles);

    //make negative image ( imadjust(imgc,[0 1], [1 0]) )
    imadjust(imagein, imgnegative, 0, Vec2i(0, 255), Vec2i(255, 0));
    threshold(imgnegative, imgnegative, 127, 255, THRESH_BINARY);

    //pad image to blocksize
    addy = imagein.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = imagein.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(imgnegative, imgnegative, 0, addy + blocksize, 0, addx + blocksize, BORDER_CONSTANT, Scalar(0));

    wavelength_mat = Mat::zeros(Size(imgnegative.cols, imgnegative.rows), CV_32FC1);

    for (int y = 0; y < imgnegative.rows - blocksize; y += blocksize) {
        for (int x = 0; x < imgnegative.cols - blocksize; x += blocksize) {
            window = imgnegative(Rect(x, y, win_width, win_height * 2));

            window_rot_mat = getRotationMatrix2D(window_center, (int)angles.at<uchar>(y, x), 1.0);
            warpAffine(window, window_rotated, window_rot_mat, window.size());

            window_rotated = window_rotated(Rect(0, win_height / 2, win_width, win_height));

            for (int j = 0; j < win_width; ++j) {
                for (int i = 0; i < win_height; ++i) {
                    xsignature[j] = xsignature[j] + (int)window_rotated.at<uchar>(i, j);
                }
            }

            findPeaks(xsignature, peaks);

            for (int i = 0; i < peaks.size(); ++i) {
                if (peaks[i] == 2 * blocksize) {
                    peaks[i] = 2 * blocksize - 1;
                }
                if (peaks[i] == 0 && i != 0) {
                    noofpeaks = i - 1;
                    break;
                }
                if (xsignature[peaks[i]] < 2000) {
                    noofpeaks = 0;
                    break;
                }
                else {}
            }

            if (noofpeaks > 1) {
                wavelength = (peaks[noofpeaks] - peaks.front()) / noofpeaks;

                if (wavelength > 4 && wavelength < 16) {
                    for (int i = 0; i < blocksize; ++i) {
                        for (int j = 0; j < blocksize; ++j) {
                            wavelength_mat.at<float>(y + i, x + j) = wavelength;
                        }
                    }
                }
                else {}

            }
            else {}

            //clear variables
            peaks.clear();

            noofpeaks = 0;

            for (int j = 0; j < win_width; ++j) {
                xsignature[j] = 0;
            }

        }
    }

    wavelength_mat_temp = wavelength_mat(Rect(0, 0, wavelength_mat.cols - (addx + blocksize), wavelength_mat.rows - (addy + blocksize)));

    wavelength_mat_temp.copyTo(wavelengthim);
}

void orientImage(InputArray image, InputArray imsegmented, OutputArray orientim, OutputArray orientimshow, int blocksize)
{
    Mat imagein, imsegment, imorient, imorient_temp, imorientshow, imorientshow_temp, block, block_rot_mat, block_rotated, accum, accumaux, line_mat, line_mat_rotated, line_rot_mat;
    Mat1b imgnegative, image_out;
    int addy, addx, angleincrement, angle, angleshow;
    double minval, maxval, lowlimitfftvalue = 90000, highlimitfftvalue = 160000;
    Point minloc, maxloc;
    Point2f block_center(blocksize / 2.0f, blocksize / 2.0f);
    Point2f line_mat_center(blocksize / 2.0f, blocksize / 2.0f);

    line_mat = Mat::zeros(Size(blocksize, blocksize), CV_8UC1);
    accum = Mat::zeros(Size(blocksize, 180), CV_32FC1);
    accumaux = Mat::zeros(Size(1, 180), CV_32FC1);
    angleincrement = 5;

    for (int j = 0; j < line_mat.cols; ++j) {
        line_mat.at<uchar>(blocksize / 2, j) = 255;
    }

    image.copyTo(imagein);
    imsegmented.copyTo(imsegment);

    //make negative image ( imadjust(imgc,[0 1], [1 0]) )
    imadjust(imagein, imgnegative, 0, Vec2i(0, 255), Vec2i(255, 0));
    threshold(imgnegative, imgnegative, 127, 255, THRESH_BINARY);

    //pad image to blocksize
    addy = imagein.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = imagein.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(imgnegative, imgnegative, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(0));
    copyMakeBorder(imsegment, imsegment, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(255));

    imorient = Mat::zeros(Size(imgnegative.cols, imgnegative.rows), CV_8UC1);
    imorientshow = Mat::zeros(Size(imgnegative.cols, imgnegative.rows), CV_8UC1);

    for (int y = 0; y < imgnegative.rows; y += blocksize) {
        for (int x = 0; x < imgnegative.cols; x += blocksize) {
            if ((int)imsegment.at<uchar>(y, x) < 10) {
                block = imgnegative(Rect(x, y, blocksize, blocksize));

                for (int ang = 0; ang < 180; ang += angleincrement) {
                    block_rot_mat = getRotationMatrix2D(block_center, ang, 1.0);
                    warpAffine(block, block_rotated, block_rot_mat, block.size());

                    for (int j = 0; j < blocksize; ++j) {
                        for (int i = 0; i < blocksize; ++i) {
                            accum.at<float>(ang, j) = accum.at<float>(ang, j) + (int)block_rotated.at<uchar>(i, j);
                        }
                    }

                    dft(accum.row(ang), accum.row(ang));
                    accum.row(ang) = abs(accum.row(ang));

                }

                reduce(accum, accumaux, 1, REDUCE_SUM, -1);

                minMaxLoc(accumaux, &minval, &maxval, &minloc, &maxloc);

                //TODO: limitar os valores da fft (maximo e minimo possivelmente)
                //angle = maxloc.y;

                if (maxval > lowlimitfftvalue && maxval < highlimitfftvalue) {
                    angle = maxloc.y;
                }
                else {
                    angle = 255;
                }
            }
            else {
                angle = 255;
            }

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    imorient.at<uchar>(y + i, x + j) = (uchar)angle;
                }
            }

            //construct lines in image
            angleshow = 90 - angle;
            line_rot_mat = getRotationMatrix2D(line_mat_center, angleshow, 1.0);
            warpAffine(line_mat, line_mat_rotated, line_rot_mat, line_mat.size());

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    imorientshow.at<uchar>(y + i, x + j) = line_mat_rotated.at<uchar>(i, j);
                }
            }

            //clear variables
            for (int i = 0; i < accum.rows; ++i) {
                for (int j = 0; j < accum.cols; ++j) {
                    accum.at<float>(i, j) = 0;
                }
            }

        }
    }

    imorientshow_temp = imorientshow(Rect(0, 0, imorientshow.cols - addx, imorientshow.rows - addy));

    imorient_temp = imorient(Rect(0, 0, imorient.cols - addx, imorient.rows - addy));

    imorientshow_temp.copyTo(orientimshow);
    imorient_temp.copyTo(orientim);
}


void freqFilter(InputArray image, OutputArray imgenhanced, int blocksize)
{
    Mat imagein, img_temp_out, block, block_rot_mat, block_rotated, accum, accumaux, window, window_rotated, window_rot_mat, gabor_filter, block_filtred, block_new, img_filtred;
    Mat1b imgnegative, img_out;
    int addx, addy, angle, noofpeaks, filtersize, angleincrement;
    int win_width = blocksize * 2;
    int win_height = blocksize;
    float wavelength, blockfreq, sigma, default_wavelength;
    Point2f block_center(blocksize / 2.0f, blocksize / 2.0f);
    Point2f window_center(win_width / 2.0f, win_height / 2.0f);
    double minval, maxval;
    Point minloc, maxloc;
    vector<float> xsignature;
    vector<int> peaks;

    xsignature.assign(win_width, 0);

    accum = Mat::zeros(Size(blocksize, 180), CV_32FC1);
    accumaux = Mat::zeros(Size(1, 180), CV_32FC1);
    noofpeaks = 0;
    default_wavelength = 10;
    wavelength = default_wavelength;
    angleincrement = 9;

    image.copyTo(imagein);

    //make negative image ( imadjust(imgc,[0 1], [1 0]) )
    imadjust(imagein, imgnegative, 0, Vec2i(0, 255), Vec2i(255, 0));

    //pad image to blocksize
    addy = imagein.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = imagein.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    //copyMakeBorder(imgnegative, imgnegative, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(0));
    //altera��o necess�ria para ganhar espa�o para criar a janela e para a convolu��o do bloco com o filtro.
    copyMakeBorder(imgnegative, imgnegative, blocksize, addy + blocksize, blocksize, addx + blocksize, BORDER_CONSTANT, Scalar(0));

    //create output image
    img_filtred = Mat::zeros(Size(imgnegative.cols, imgnegative.rows), CV_8UC1);

    //for each block in image, exclude pad
    for (int y = blocksize; y < imgnegative.rows - blocksize; y += blocksize) {
        for (int x = blocksize; x < imgnegative.cols - blocksize; x += blocksize) {
            block = imgnegative(Rect(x, y, blocksize, blocksize));

            for (int ang = 0; ang < 180; ang += angleincrement) {
                block_rot_mat = getRotationMatrix2D(block_center, ang, 1.0);
                warpAffine(block, block_rotated, block_rot_mat, block.size());

                for (int j = 0; j < blocksize; ++j) {
                    for (int i = 0; i < blocksize; ++i) {
                        accum.at<float>(ang, j) = accum.at<float>(ang, j) + (int)block_rotated.at<uchar>(i, j);
                    }
                }

                dft(accum.row(ang), accum.row(ang));
                accum.row(ang) = abs(accum.row(ang));

            }

            reduce(accum, accumaux, 1, REDUCE_SUM, -1);

            minMaxLoc(accumaux, &minval, &maxval, &minloc, &maxloc);
            angle = maxloc.y;
            //angle = 90 - angle; //necess�rio para desenhar a imagem de orienta��o

            window = imgnegative(Rect(x, y, win_width, win_height * 2));

            window_rot_mat = getRotationMatrix2D(window_center, angle, 1.0);
            warpAffine(window, window_rotated, window_rot_mat, window.size());

            window_rotated = window_rotated(Rect(0, win_height / 2, win_width, win_height));

            for (int j = 0; j < win_width; ++j) {
                for (int i = 0; i < win_height; ++i) {
                    xsignature[j] = xsignature[j] + (int)window_rotated.at<uchar>(i, j);
                }
            }

            findPeaks(xsignature, peaks);

            for (int i = 0; i < peaks.size(); ++i) {
                if (peaks[i] == 2 * blocksize) {
                    peaks[i] = 2 * blocksize - 1;
                }
                if (peaks[i] == 0 && i != 0) {
                    noofpeaks = i - 1;
                    break;
                }
                if (xsignature[peaks[i]] < 2000) {
                    noofpeaks = 0;
                    break;
                }
                else {}
            }

            if (noofpeaks > 1) {
                wavelength = (peaks[noofpeaks] - peaks.front()) / noofpeaks;
            }
            else {
                wavelength = default_wavelength;
            }

            sigma = wavelength * 0.35f;

            filtersize = round(4 * sigma);

            gabor_filter = getGaborKernel(Size(filtersize, filtersize), sigma, (angle * CV_PI / 180), wavelength, 1, 0); //CV_64F

            filter2D(block, block_filtred, -1, gabor_filter, Point(-1, -1), 0, BORDER_DEFAULT);

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    img_filtred.at<uchar>(y + i, x + j) = block_filtred.at<uchar>(i, j);
                }
            }

            /*block_new = imgnegative(Rect(x - round(blocksize / 2), y - round(blocksize / 2), blocksize + round(blocksize / 2), blocksize + round(blocksize / 2)));

            filter2D(block_new, block_filtred, -1, gabor_filter, Point(-1,-1), 0, BORDER_DEFAULT);

            for (int i = round(blocksize / 2); i < (2*blocksize - round(blocksize / 2)); ++i) {
                for (int j = round(blocksize / 2); j < (2*blocksize - round(blocksize / 2)); ++j) {
                    img_filtred.at<uchar>(y + i - round(blocksize / 2),x + j - round(blocksize / 2)) = block_filtred.at<uchar>(i, j);
                }
            }*/

            //clear variables
            peaks.clear();

            noofpeaks = 0;

            for (int j = 0; j < win_width; ++j) {
                xsignature[j] = 0;
            }

            for (int i = 0; i < accum.rows; ++i) {
                for (int j = 0; j < accum.cols; ++j) {
                    accum.at<float>(i, j) = 0;
                }
            }

        }
    }

    img_temp_out = img_filtred(Rect(blocksize, blocksize, img_filtred.cols - (addx + 2 * blocksize), img_filtred.rows - (addy + 2 * blocksize)));

    imadjust(img_temp_out, img_out, 0, Vec2i(0, 255), Vec2i(255, 0)); // extract minutiae from valleys
    //img_temp_out.copyTo(img_out); // extract from ridge

    img_out.copyTo(imgenhanced);
}

void imadjust(const Mat1b& src, Mat1b& dst, int tol, Vec2i in, Vec2i out)
{
    // src : input CV_8UC1 image
    // dst : output CV_8UC1 imge
    // tol : tolerance, from 0 to 100.
    // in  : src image bounds
    // out : dst image buonds

    dst = src.clone();

    tol = max(0, min(100, tol));

    if (tol > 0)
    {
        // Compute in and out limits

        // Histogram
        vector<int> hist(256, 0);
        for (int r = 0; r < src.rows; ++r) {
            for (int c = 0; c < src.cols; ++c) {
                hist[src(r, c)]++;
            }
        }

        // Cumulative histogram
        vector<int> cum = hist;
        for (int i = 1; i < hist.size(); ++i) {
            cum[i] = cum[i - 1] + hist[i];
        }

        // Compute bounds
        int total = src.rows * src.cols;
        int low_bound = total * tol / 100;
        int upp_bound = total * (100 - tol) / 100;
        in[0] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), low_bound));
        in[1] = distance(cum.begin(), lower_bound(cum.begin(), cum.end(), upp_bound));

    }

    // Stretching
    float scale = float(out[1] - out[0]) / float(in[1] - in[0]);
    for (int r = 0; r < dst.rows; ++r)
    {
        for (int c = 0; c < dst.cols; ++c)
        {
            int vs = max(src(r, c) - in[0], 0);
            int vd;
            if (scale < 0) {
                vd = max(int(vs * scale + 0.5f) + out[0], out[1]);
            }
            else {
                vd = min(int(vs * scale + 0.5f) + out[0], out[1]);
            }
            dst(r, c) = saturate_cast<uchar>(vd);
        }
    }
}

void stdDevImage(InputArray image, OutputArray mask, int blocksize, float& stddevfullimg)
{
    Mat stddevim, stddevim_temp;
    Mat aux = Mat::zeros(Size(blocksize, blocksize), CV_32FC1);
    Scalar mean, stddev;
    float stddev_fullimg;
    int addy, addx;

    image.copyTo(stddevim);
    stddevim.convertTo(stddevim, CV_32FC1);

    //pad image to blocksize
    addy = stddevim.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = stddevim.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(stddevim, stddevim, 0, addy, 0, addx, BORDER_CONSTANT, Scalar(255));

    stddev_fullimg = 0;

    for (int y = 0; y < stddevim.rows; y += blocksize) {
        for (int x = 0; x < stddevim.cols; x += blocksize) {
            aux = stddevim(Rect(x, y, blocksize, blocksize));

            meanStdDev(aux, mean, stddev); //calculate stddev to populate / desired value in mean[0] and stddev[0]

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    stddevim.at<float>(y + i, x + j) = stddev[0];
                }
            }
        }
    }

    for (int y = 0; y < stddevim.rows; y += blocksize) {
        for (int x = 0; x < stddevim.cols; x += blocksize) {
            stddev_fullimg = stddev_fullimg + (stddevim.at<float>(y, x) / 255.0f);
        }
    }

    stddev_fullimg = stddev_fullimg / ((stddevim.rows / blocksize) + (stddevim.cols / blocksize));

    stddevfullimg = stddev_fullimg;

    stddevim_temp = stddevim(Rect(0, 0, stddevim.cols - addx, stddevim.rows - addy));

    stddevim_temp.copyTo(mask);
}

void normaliseMeanStd(InputArray image, OutputArray imgnorm, float reqmean, float reqvar)
{
    Mat temp;
    Scalar mean, stddev;

    image.copyTo(temp);

    temp.convertTo(temp, CV_32FC1, 1.0f / 255.0f);

    meanStdDev(temp, mean, stddev);

    temp -= mean[0];
    temp /= stddev[0];

    temp = reqmean + (sqrt(reqvar) * temp);

    /*for (int i = 0; i < temp.rows; ++i) {
        for (int j = 0; j < temp.cols; ++j) {
            if ( temp.at<float>(i,j) > mean[0] ) {
                temp.at<float>(i, j) = reqmean + (temp.at<float>(i, j) - mean[0] * sqrt(reqvar / stddev[0]));
            }
            else {
                temp.at<float>(i, j) = reqmean - (temp.at<float>(i, j) - mean[0] * sqrt(reqvar / stddev[0]));
            }
        }
    }*/

    temp.convertTo(temp, CV_8UC1, 255);

    temp.copyTo(imgnorm);
}

void normFloatandConvert8U(InputArray imageFloat, OutputArray image8u)
{
    Mat temp;
    double min=5, max=15;

    imageFloat.copyTo(temp);

    //minMaxLoc(temp, &min, &max);

    temp -= min;
    temp /= max - min;

    temp.convertTo(temp, CV_8UC1, 255);

    temp.copyTo(image8u);
}

void findMinutiae(InputArray image, InputArray imthin, InputArray imorient, InputArray imsegmented, OutputArray mapmin, string dirimages, string fpname, float stddevimthreshold, int blocksize, float meanwavelength, int fpclassification)
{
    Mat imagein, temp, orientim, stddevim, segmented;
    Mat3b minMap;
    Mat1b segmentednegative;
    ofstream minutiaesfile, dirmapfile;
    float CN, meanangle, poincare, qualityscore, stddev_fullimg, differences_mean;
    int countEdge = 0, countBifur = 0, countMinu = 0, t1 = 0, t2 = 0, anglesteps = 0, currentangle, previousangle, fowardangle, topangle, bottomangle, firstangle, secondangle, thirdangle, fourthangle, maxneighbor, angle, next, nextnext, corex, corey;

    vector<Point2i> cores;
    vector<int> coreflag; //if flag == 1 remove point
    vector<int> coreneighbor; //amount of points around the point
    float dist;

    vector<vector<Point>> contours;
    vector<double> contour_areas;
    vector<Vec4i> hierarchy;
    int max_area;
    Moments mu;
    Point2f mc;

    vector<int> angles(8, 0), poscorex, poscorexaux, poscorey, findposcorex, numdots, ylimit, newylimit, differences;
    vector<vector<int>> poscorexuniqs;
    vector<int>::iterator poscorexit, max_numdots, ylimitmax, ylimitmin, maxdifference;

    imthin.copyTo(temp);
    image.copyTo(imagein);
    imorient.copyTo(orientim);
    imsegmented.copyTo(segmented);

    minutiaesfile.open(dirimages + "Filtradas/" + removeExtension(fpname) + ".min");
    dirmapfile.open(dirimages + "Filtradas/" + removeExtension(fpname) + ".dir");

    //imwrite(dirimages + "Filtradas/imseg_" + fpname, segmented);

    stdDevImage(imagein, stddevim, blocksize, stddev_fullimg);
    normFloatandConvert8U(stddevim, stddevim);

    //imwrite(dirimages + "Filtradas/qual_" + fpname, stddevim);

    cvtColor(imagein, minMap, COLOR_GRAY2BGR);

    temp = temp / 255;
    CN = 0;
    //fpclassification = 1; //1=espiral,2=le,3=ld,4=arco
    corex = 0;
    corey = 0;

    for (int y = 0; y < temp.rows; y += blocksize) {
        for (int x = 0; x < temp.cols; x += blocksize) {
            dirmapfile << y / blocksize << " , " << x / blocksize << " : " << (int)orientim.at<uchar>(y, x) << endl;
        }
    }

    for (int y = blocksize; y < temp.rows - blocksize; ++y) {
        for (int x = blocksize; x < temp.cols - blocksize; ++x) {

            if ((y % blocksize == 0) && (x % blocksize == 0) && (x > 2 * blocksize) && (x < temp.cols - 2 * blocksize))
            {
                if (((int)segmented.at<uchar>(y, x) < 10)) {
                    poincare = 0;

                    for (int aux = 0; aux < 8; ++aux) {
                        t1 = angleposition(orientim, x, y, aux, blocksize);
                        t2 = angleposition(orientim, x, y, aux + 1, blocksize);
                        if ((t1 - t2) > 90) {
                            t1 = t1 + 180;
                        }
                        if ((t1 - t2) < -90) {
                            t2 = t2 + 180;
                        }
                        poincare = poincare + t1 - t2;
                    }

                    if (poincare == 180 || poincare == 360) {
                        cores.push_back(Point2i(x, y));
                        coreflag.push_back(0);
                        coreneighbor.push_back(0);
                        //rectangle(minMap, Rect(x, y, blocksize, blocksize), Scalar(0, 255, 255), 1);
                    }
                }
            }

            if ((int)temp.at<uchar>(y, x) > 0) {
                if (((int)stddevim.at<uchar>(y, x) > (255 * stddevimthreshold)) && ((int)segmented.at<uchar>(y, x) < 10)) {
                    qualityscore = (int)stddevim.at<uchar>(y, x) / 255.0f;

                    currentangle = (int)orientim.at<uchar>(y, x);
                    /*previousangle = angleposition(orientim, x, y, 1, blocksize);
                    fowardangle = angleposition(orientim, x, y, 5, blocksize);
                    topangle = angleposition(orientim, x, y, 3, blocksize);
                    bottomangle = angleposition(orientim, x, y, 7, blocksize);

                    meanangle = (previousangle + fowardangle + topangle + bottomangle) / 4;*/

                    /*for (int i = 0; i < 8; ++i) {
                        angles[i] = angleposition(orientim, x, y, i, blocksize);
                    }
                    sort(angles.begin(),angles.end());

                    meanangle = (angles[(angles.size() / 2) - 1] + angles[(angles.size() / 2)]) / 2;*/

                    fowardangle = 0;

                    for (int i = 0; i < 8; ++i) {
                        angles[i] = angleposition(orientim, x, y, i, blocksize);
                        previousangle = abs(currentangle - angles[i]);
                        if (previousangle > 20 && previousangle < 160) {
                            fowardangle++;
                        }
                    }

                    if (fowardangle > 4) {
                        //continue;
                        qualityscore /= 2;
                    }

                    CN = 0;

                    for (int aux = 1; aux < 9; ++aux) {
                        t1 = position(temp, x, y, aux);
                        t2 = position(temp, x, y, aux + 1);
                        CN = CN + abs(t1 - t2);
                    }
                    CN = CN / 2;

                    if (CN == 1) {
                        rectangle(minMap, Rect(x - 3, y - 3, 6, 6), Scalar(0, 0, 255), 1);
                        //anglesteps = round((int)orientim.at<uchar>(y, x) / 11.25);
                        anglesteps = (int)orientim.at<uchar>(y, x);
                        minutiaesfile << countMinu << " : " << x << " , " << y << " : " << anglesteps << " : " << qualityscore << " : " << "RIG" << " : " << meanwavelength << endl;
                        countEdge++;
                        countMinu++;
                    }
                    else if (CN == 3) {
                        rectangle(minMap, Rect(x - 3, y - 3, 6, 6), Scalar(255, 0, 0), 1);
                        //anglesteps = round((int)orientim.at<uchar>(y, x) / 11.25);
                        anglesteps = (int)orientim.at<uchar>(y, x);
                        minutiaesfile << countMinu << " : " << x << " , " << y << " : " << anglesteps << " : " << qualityscore << " : " << "BIF" << " : " << meanwavelength << endl;
                        countBifur++;
                        countMinu++;
                    }
                    else {}
                }
                else {}
            }
        }
    }

    if (!cores.empty()) {
        for (int i = 0; i < cores.size(); ++i) {
            for (int j = 0; j < cores.size(); ++j) {
                if (i != j) {
                    dist = sqrt(pow((cores[i].x - cores[j].x), 2) + pow((cores[i].y - cores[j].y), 2));
                    if (dist < 3 * blocksize / 2) {
                        coreneighbor[i]++;
                    }
                }
            }
        }

        maxneighbor = *max_element(coreneighbor.begin(), coreneighbor.end());
    }
    else {
        maxneighbor = 0;
    }

    if (maxneighbor > 2 && fpclassification != 4) { //maxneighbor > 2 && fpclassification != 4
        if (maxneighbor == 3) {
            for (int i = 0; i < cores.size(); ++i) {
                if (coreneighbor[i] < 2) {
                    coreflag[i] = 1;
                }
            }
        }
        else {
            for (int i = 0; i < cores.size(); ++i) {
                if (coreneighbor[i] < 3) {
                    coreflag[i] = 1;
                }
            }
        }

        for (int i = 0; i < cores.size(); ++i) {
            if (coreflag[i] == 1) {
                cores.erase(cores.begin() + i);
                coreflag.erase(coreflag.begin() + i);
                coreneighbor.erase(coreneighbor.begin() + i);
                --i;
            }
        }

        corex = cores[0].x + blocksize;
        corey = cores[0].y + blocksize;
    }
    else if (maxneighbor > 2 && fpclassification == 4) { //tem poincare e � arco
        if (maxneighbor == 3) {
            for (int i = 0; i < cores.size(); ++i) {
                if (coreneighbor[i] < 2) {
                    coreflag[i] = 1;
                }
            }
        }
        else {
            for (int i = 0; i < cores.size(); ++i) {
                if (coreneighbor[i] < 3) {
                    coreflag[i] = 1;
                }
            }
        }

        for (int i = 0; i < cores.size(); ++i) {
            if (coreflag[i] == 1) {
                cores.erase(cores.begin() + i);
                coreflag.erase(coreflag.begin() + i);
                coreneighbor.erase(coreneighbor.begin() + i);
                --i;
            }
        }

        for (int y = 0; y < temp.rows; y += blocksize) {
            for (int x = 0; x < temp.cols - 2 * blocksize; x += blocksize) {
                angle = (int)orientim.at<uchar>(y, x);
                next = (int)orientim.at<uchar>(y, x + blocksize);
                nextnext = (int)orientim.at<uchar>(y, x + 2 * blocksize);
                if ((angle < 90 && next > 90) || (angle < 90 && nextnext > 90)) {
                    poscorex.push_back(x);
                    poscorey.push_back(y);
                }
            }
        }
        poscorexaux = poscorex;

        sort(poscorexaux.begin(), poscorexaux.end());
        poscorexaux.erase(unique(poscorexaux.begin(), poscorexaux.end()), poscorexaux.end());

        for (int i = 0; i < poscorexaux.size(); ++i) {
            poscorexit = find(poscorex.begin(), poscorex.end(), poscorexaux[i]);
            while (poscorexit != poscorex.end()) {
                findposcorex.push_back(poscorexit - poscorex.begin());
                poscorexit = find(poscorexit + 1, poscorex.end(), poscorexaux[i]);
            }
            poscorexuniqs.push_back(findposcorex);
            numdots.push_back(findposcorex.size());

            findposcorex.clear();
        }

        max_numdots = max_element(numdots.begin(), numdots.end());
        corex = poscorex[poscorexuniqs[max_numdots - numdots.begin()][0]];

        corex = corex + blocksize;
        corey = cores[0].y + blocksize;

        cout << "Poincare e arco\n";
    }
    else if (maxneighbor < 3 && fpclassification == 4) { // maxneighbor < 3 && fpclassification == 4
        for (int y = 0; y < temp.rows; y += blocksize) {
            for (int x = 0; x < temp.cols - 2 * blocksize; x += blocksize) {
                angle = (int)orientim.at<uchar>(y, x);
                next = (int)orientim.at<uchar>(y, x + blocksize);
                nextnext = (int)orientim.at<uchar>(y, x + 2 * blocksize);
                if ((angle < 90 && next > 90) || (angle < 90 && nextnext > 90)) {
                    poscorex.push_back(x);
                    poscorey.push_back(y);
                }
            }
        }
        poscorexaux = poscorex;

        sort(poscorexaux.begin(), poscorexaux.end());
        poscorexaux.erase(unique(poscorexaux.begin(), poscorexaux.end()), poscorexaux.end());

        for (int i = 0; i < poscorexaux.size(); ++i) {
            poscorexit = find(poscorex.begin(), poscorex.end(), poscorexaux[i]);
            while (poscorexit != poscorex.end()) {
                findposcorex.push_back(poscorexit - poscorex.begin());
                poscorexit = find(poscorexit + 1, poscorex.end(), poscorexaux[i]);
            }
            poscorexuniqs.push_back(findposcorex);
            numdots.push_back(findposcorex.size());

            findposcorex.clear();
        }

        max_numdots = max_element(numdots.begin(), numdots.end());

        for (int i = 0; i < poscorexuniqs[max_numdots - numdots.begin()].size(); ++i) {
            ylimit.push_back(poscorey[poscorexuniqs[max_numdots - numdots.begin()][i]]);
        }

        if (ylimit.size() > 7) {
            for (int i = 3; i < ylimit.size() - 3; ++i) {
                newylimit.push_back(ylimit[i]);
            }
        }
        else if (ylimit.size() > 5) {
            for (int i = 2; i < ylimit.size() - 2; ++i) {
                newylimit.push_back(ylimit[i]);
            }
        }
        else if (ylimit.size() > 3) {
            for (int i = 1; i < ylimit.size() - 1; ++i) {
                newylimit.push_back(ylimit[i]);
            }
        }
        else {
            newylimit = ylimit;
        }

        ylimitmax = max_element(newylimit.begin(), newylimit.end());
        ylimitmin = min_element(newylimit.begin(), newylimit.end());

        for (int i = *ylimitmin; i < *ylimitmax; i += blocksize) {
            angle = (int)orientim.at<uchar>(i, poscorex[poscorexuniqs[max_numdots - numdots.begin()][0]]);
            next = (int)orientim.at<uchar>(i, poscorex[poscorexuniqs[max_numdots - numdots.begin()][0]] + blocksize);
            nextnext = (int)orientim.at<uchar>(i, poscorex[poscorexuniqs[max_numdots - numdots.begin()][0]] + 2 * blocksize);
            if (abs(next - angle) > abs(nextnext - angle)) {
                differences.push_back(abs(next - angle));
            }
            else {
                differences.push_back(abs(nextnext - angle));
            }
        }

        if (differences.size() != 0) {
            differences_mean = accumulate(differences.begin(), differences.end(), 0.0f) / differences.size();
        }
        else {
            differences_mean = 0;
        }

        if (differences_mean > 60) {
            maxdifference = max_element(differences.begin(), differences.end());
            corex = poscorex[poscorexuniqs[max_numdots - numdots.begin()][0]];
            corey = *ylimitmin + (maxdifference - differences.begin()) * blocksize;
        }
        else {
            corex = poscorex[poscorexuniqs[max_numdots - numdots.begin()][0]];
            corey = round(*ylimitmin + *ylimitmax) / 2;
        }

        corex = corex + blocksize;
        corey = corey + blocksize;

        cout << "Sem poincare e arco\n";
    }
    else {
        imadjust(segmented, segmentednegative, 0, Vec2i(0, 255), Vec2i(255, 0));
        threshold(segmentednegative, segmentednegative, 127, 255, THRESH_BINARY);

        //imwrite(dirimages + "Filtradas/imseg_" + fpname, segmentednegative);

        findContours(segmentednegative, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);

        for (int i = 0; i < contours.size(); ++i) {
            contour_areas.push_back(contourArea(contours[i]));
        }
        max_area = max_element(contour_areas.begin(), contour_areas.end()) - contour_areas.begin();

        mu = moments(contours[max_area]);
        mc = Point2f(static_cast<float>(mu.m10 / (mu.m00 + 1e-5)), static_cast<float>(mu.m01 / (mu.m00 + 1e-5)));

        corex = round(mc.x);
        corey = round(mc.y);
    }

    if ((corex - 2 * blocksize) <= 0) {
        firstangle = (int)orientim.at<uchar>(corey, corex);
        secondangle = (int)orientim.at<uchar>(corey, corex + 2 * blocksize);
    }
    else if ((corex + 2 * blocksize) >= orientim.cols) {
        firstangle = (int)orientim.at<uchar>(corey, corex - 2 * blocksize);
        secondangle = (int)orientim.at<uchar>(corey, corex);
    }
    else {
        firstangle = (int)orientim.at<uchar>(corey, corex - 2 * blocksize);
        secondangle = (int)orientim.at<uchar>(corey, corex + 2 * blocksize);
    }

    if (firstangle > 90) {
        for (int i = corex - 3 * blocksize; i > 0; i -= blocksize) {
            firstangle = (int)orientim.at<uchar>(corey, i);
            if (firstangle < 90) {
                break;
            }
        }
    }

    if (secondangle < 90) {
        for (int i = corex + 4 * blocksize; i < temp.cols; i += blocksize) {
            secondangle = (int)orientim.at<uchar>(corey, i);
            if (secondangle > 90) {
                break;
            }
        }
    }

    if (firstangle > 90 || secondangle < 90) {
        firstangle = 90;
        secondangle = 90;
    }

    anglesteps = (firstangle + secondangle) / 2;
    anglesteps -= 90;
    //anglesteps = floor(anglesteps / 11.25);

    qualityscore = (int)stddevim.at<uchar>(corey, corex) / 255.0f;

    rectangle(minMap, Rect(corex - 3, corey - 3, 6, 6), Scalar(0, 255, 255), 1);
    minutiaesfile << countMinu << " : " << corex << " , " << corey << " : " << anglesteps << " : " << qualityscore << " : " << "CORE" << " : " << meanwavelength << endl;
    //dirmapfile << "Core : " << corey << endl; //TODO: Remover isso quando os testes acabarem
    //dirmapfile << "stddev : " << stddev_fullimg << endl; //TODO: Remover isso quando os testes acabarem

    minutiaesfile.close();
    dirmapfile.close();
    minMap.copyTo(mapmin);
    cout << "Edges: " << countEdge << endl << "Bifurcations: " << countBifur << endl;
}

int angleposition(InputArray imorient, int x, int y, int current, int blocksize)
{
    /*
    get angle value based on chart :
    2 | 3 | 4
    1 |   | 5
    0 | 7 | 6
    */
    Mat temp;

    imorient.copyTo(temp);

    switch (current)
    {
    case(0):
    case(8):
        return (int)temp.at<uchar>(y + blocksize, x - blocksize);
    case(1):
        return (int)temp.at<uchar>(y, x - blocksize);
    case(2):
        return (int)temp.at<uchar>(y - blocksize, x - blocksize);
    case(3):
        return (int)temp.at<uchar>(y - blocksize, x);
    case(4):
        return (int)temp.at<uchar>(y - blocksize, x + blocksize);
    case(5):
        return (int)temp.at<uchar>(y, x + blocksize);
    case(6):
        return (int)temp.at<uchar>(y + blocksize, x + blocksize);
    case(7):
        return (int)temp.at<uchar>(y + blocksize, x);
    default:
        break;
    }
    return 0;
}

int position(InputArray image, int x, int y, int current)
{
    /*
    get pixel value based on chart :
    4 | 3 | 2
    5 |   | 1
    6 | 7 | 8
    */
    Mat temp;
    image.copyTo(temp);
    switch (current)
    {
    case(1):
    case(9):
        return (int)temp.at<uchar>(y, x + 1);
    case(2):
        return (int)temp.at<uchar>(y - 1, x + 1);
    case(3):
        return (int)temp.at<uchar>(y - 1, x);
    case(4):
        return (int)temp.at<uchar>(y - 1, x - 1);
    case(5):
        return (int)temp.at<uchar>(y, x - 1);
    case(6):
        return (int)temp.at<uchar>(y + 1, x - 1);
    case(7):
        return (int)temp.at<uchar>(y + 1, x);
    case(8):
        return (int)temp.at<uchar>(y + 1, x + 1);
    default:
        break;
    }
    return 0;
}

void imageEnhance_v3(InputArray image, OutputArray imgenhanced)
{
    Mat element;
    Mat imgbinotsu;
    Mat imgbin;
    Mat imgblur;
    Mat imgthin;
    Mat imglineerode;
    Mat imgmask;
    Mat imgenh;
    Mat imgout;

    threshold(image, imgbinotsu, 0, 255, THRESH_OTSU);
    adaptiveThreshold(image, imgbin, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 15, 2);

    bitwise_or(imgbin, imgbinotsu, imgbin);

    // your input binary image
    // assuming that blob pixels have positive values, zero otherwise
    // threashold specifying minimum area of a blob
    double size_threshold = 12;

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    vector<int> small_blobs;
    double contour_area;
    Mat temp_image;

    // find all contours in the binary image
    imgbin.copyTo(temp_image);
    findContours(temp_image, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
    // Find indices of contours whose area is less than threshold
    if (!contours.empty()) {
        for (size_t i = 0; i < contours.size(); ++i) {
            contour_area = contourArea(contours[i]);
            if (contour_area < size_threshold)
                small_blobs.push_back(i);
        }
    }

    // fill-in all small contours with zeros
    for (size_t i = 0; i < small_blobs.size(); ++i) {
        drawContours(imgbin, contours, small_blobs[i], Scalar(0), FILLED, 8);
    }


    //medianBlur(imgbin, imgblur, 3);
    //GaussianBlur(imgbin, imgblur, Size(5, 5), 1, 1);
    imgbin.copyTo(imgblur);

    imgblur.copyTo(imgthin);

    //create vertical line and rotate 1:90 degrees
    Mat line = Mat::zeros(Size(5, 5), CV_8UC1);
    Mat rot_mat;
    Mat rotated;
    Point2f line_center((line.cols - 1) / 2.0f, (line.rows - 1) / 2.0f);

    imglineerode = Mat::zeros(Size(imgthin.cols, imgthin.rows), CV_8UC1);

    for (size_t i = 0; i < line.rows; ++i) {
        line.at<uchar>(i, (line.cols - 1) / 2) = 1;
    }

    for (size_t i = 0; i <= 180; ++i) {
        rot_mat = getRotationMatrix2D(line_center, i, 1.0);
        warpAffine(line, rotated, rot_mat, line.size());
        morphologyEx(imgthin, temp_image, MORPH_ERODE, rotated, Point(-1, -1), 1);
        bitwise_or(temp_image, imglineerode, imglineerode);
    }

    imglineerode.copyTo(imgenh);

    imgenh.copyTo(imgenhanced);
}

void imageEnhance_v2(InputArray image, OutputArray imgenhanced)
{
    Mat element;
    Mat imgbinotsu;
    Mat imgbin;
    Mat imgblur;
    Mat imgthin;
    Mat imglineerode;
    Mat imgmask;
    Mat imgenh;
    Mat imgout;

    threshold(image, imgbinotsu, 0, 255, THRESH_OTSU);
    adaptiveThreshold(image, imgbin, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 15, 2);

    bitwise_or(imgbin, imgbinotsu, imgbin);

    // your input binary image
    // assuming that blob pixels have positive values, zero otherwise
    // threashold specifying minimum area of a blob
    double size_threshold = 10;

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    vector<int> small_blobs;
    double contour_area;
    Mat temp_image;

    // find all contours in the binary image
    imgbin.copyTo(temp_image);
    findContours(temp_image, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
    // Find indices of contours whose area is less than threshold
    if (!contours.empty()) {
        for (size_t i = 0; i < contours.size(); ++i) {
            contour_area = contourArea(contours[i]);
            if (contour_area < size_threshold)
                small_blobs.push_back(i);
        }
    }

    // fill-in all small contours with zeros
    for (size_t i = 0; i < small_blobs.size(); ++i) {
        drawContours(imgbin, contours, small_blobs[i], Scalar(0), FILLED, 8);
    }


    //medianBlur(imgbin, imgblur, 5);
    //GaussianBlur(imgbin, imgblur, Size(5, 5), 1, 1);
    imgbin.copyTo(imgblur);

    //imshow("Imagem filtrada pela m�dia", imgblur);

    //adaptiveThreshold(imgblur, imgblur, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, 0);

    //teste de thinning
    //thinning(imgblur, imgthin, ximgproc::THINNING_GUOHALL);

    //imgblur.copyTo(imgthin);
    //element = getStructuringElement(MORPH_ELLIPSE, Size(2, 2), Point(-1, -1));
    //morphologyEx(imgblur, imgthin, MORPH_ERODE, element, Point(-1, -1), 1);
    imgblur.copyTo(imgthin);

    //teste de thinning

    //create vertical line and rotate 1:90 degrees
    Mat line = Mat::zeros(Size(5, 5), CV_8UC1);
    Mat rot_mat;
    Mat rotated;
    Point2f line_center((line.cols - 1) / 2.0f, (line.rows - 1) / 2.0f);

    imglineerode = Mat::zeros(Size(imgthin.cols, imgthin.rows), CV_8UC1);

    for (size_t i = 0; i < line.rows; ++i) {
        line.at<uchar>(i, (line.cols - 1) / 2) = 1;
    }

    for (size_t i = 0; i <= 180; ++i) {
        rot_mat = getRotationMatrix2D(line_center, i, 1.0);
        warpAffine(line, rotated, rot_mat, line.size());
        morphologyEx(imgthin, temp_image, MORPH_ERODE, rotated, Point(-1, -1), 1);
        bitwise_or(temp_image, imglineerode, imglineerode);
    }
    //create vertical line and rotate 1:90 degrees

    /*element = Mat::zeros(Size(3, 3), CV_8UC1);

    for (size_t i = 0; i < element.rows; ++i) {
        element.at<uchar>(i, (element.cols - 1) / 2) = 1;
    }

    morphologyEx(imglineerode, imgmask, MORPH_DILATE, element, Point(-1, -1), 1);

    element = Mat::zeros(Size(3, 3), CV_8UC1);

    for (size_t j = 0; j < element.cols; ++j) {
        element.at<uchar>((element.cols - 1) / 2, j) = 1;
    }

    morphologyEx(imgmask, imgmask, MORPH_DILATE, element, Point(-1, -1), 1);*/

    //multiply(image, imglineerode, imgenh, 1.0f);
    imglineerode.copyTo(imgenh);

    //imgout = thinning(imgenh);
    //thinning(imgenh, imgout, ximgproc::THINNING_GUOHALL);

    imgenh.copyTo(imgenhanced);
}

void imageEnhance_v1(InputArray image, OutputArray imgenhanced)
{
    Mat element;
    Mat imgbin;
    Mat imgblur;
    Mat imgthin;
    Mat imglineerode;
    Mat imgmask;
    Mat imgenh;

    adaptiveThreshold(image, imgbin, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 7, 0);

    // your input binary image
    // assuming that blob pixels have positive values, zero otherwise
    // threashold specifying minimum area of a blob
    double size_threshold = 20;

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    vector<int> small_blobs;
    double contour_area;
    Mat temp_image;

    // find all contours in the binary image
    imgbin.copyTo(temp_image);
    findContours(temp_image, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
    // Find indices of contours whose area is less than threshold
    if (!contours.empty()) {
        for (size_t i = 0; i < contours.size(); ++i) {
            contour_area = contourArea(contours[i]);
            if (contour_area < size_threshold)
                small_blobs.push_back(i);
        }
    }

    // fill-in all small contours with zeros
    for (size_t i = 0; i < small_blobs.size(); ++i) {
        drawContours(imgbin, contours, small_blobs[i], Scalar(0), FILLED, 8);
    }


    medianBlur(imgbin, imgblur, 5);
    //GaussianBlur(imgbin, imgblur, Size(5, 5), 1, 1);
    //imgbin.copyTo(imgblur);

    //imshow("Imagem filtrada pela m�dia", imgblur);

    adaptiveThreshold(imgblur, imgblur, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 7, 0);

    //teste de thinning
    //thinning(imgblur, imgthin, ximgproc::THINNING_ZHANGSUEN);

    //imgblur.copyTo(imgthin);
    element = getStructuringElement(MORPH_ELLIPSE, Size(2, 2), Point(-1, -1));
    morphologyEx(imgblur, imgthin, MORPH_ERODE, element, Point(-1, -1), 1);

    //teste de thinning

    //create vertical line and rotate 1:90 degrees
    Mat line = Mat::zeros(Size(5, 5), CV_8UC1);
    Mat rot_mat;
    Mat rotated;
    Point2f line_center((line.cols - 1) / 2.0f, (line.rows - 1) / 2.0f);

    imglineerode = Mat::zeros(Size(imgthin.cols, imgthin.rows), CV_8UC1);

    for (size_t i = 0; i < line.rows; ++i) {
        line.at<uchar>(i, (line.cols - 1) / 2) = 1;
    }

    for (size_t i = 0; i <= 180; ++i) {
        rot_mat = getRotationMatrix2D(line_center, i, 1.0);
        warpAffine(line, rotated, rot_mat, line.size());
        morphologyEx(imgthin, temp_image, MORPH_ERODE, rotated, Point(-1, -1), 1);
        bitwise_or(temp_image, imglineerode, imglineerode);
    }
    //create vertical line and rotate 1:90 degrees

    element = Mat::zeros(Size(3, 3), CV_8UC1);

    for (size_t i = 0; i < element.rows; ++i) {
        element.at<uchar>(i, (element.cols - 1) / 2) = 1;
    }

    morphologyEx(imglineerode, imgmask, MORPH_DILATE, element, Point(-1, -1), 1);

    element = Mat::zeros(Size(3, 3), CV_8UC1);

    for (size_t j = 0; j < element.cols; ++j) {
        element.at<uchar>((element.cols - 1) / 2, j) = 1;
    }

    morphologyEx(imgmask, imgmask, MORPH_DILATE, element, Point(-1, -1), 1);

    multiply(image, imgmask, imgenh, 1.0f);

    Mat imgout;
    thinning(imgenh, imgout, ximgproc::THINNING_ZHANGSUEN);

    imgout.copyTo(imgenhanced);
}

