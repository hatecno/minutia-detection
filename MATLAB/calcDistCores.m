directoryalignmat = "C:\Users\Fer_M\Documents\Biometria\Scanner_OCT\fingerprint\apagar\Base\ARCOSNOVOS\resized\Cores\MatrizAli\";
directorycores = "C:\Users\Fer_M\Documents\Biometria\Scanner_OCT\fingerprint\apagar\Base\ARCOSNOVOS\resized\Cores\";
directoryimgs = "C:\Users\Fer_M\Documents\Biometria\Scanner_OCT\fingerprint\apagar\Base\ARCOSNOVOS\resized\";
allfiles = dir(directoryalignmat);
fileid = fopen(directoryalignmat+"results.txt",'w');

for imgs=1:size(allfiles,1)
    if allfiles(imgs).isdir
        continue;
    else
        splitted = split(char(allfiles(imgs).name),".");
        if splitted(2) ~= "mat"
            continue;
        end
    end
    
    file = char(allfiles(imgs).name);
    splitted2=split(char(splitted(1)),"_");
    
    file1=splitted2(1)+"_"+splitted2(2)+".txt";
    file2=splitted2(3)+"_"+splitted2(4)+".txt";
%     img1=splitted2(1)+"_"+splitted2(2)+".bmp";
%     img2=splitted2(3)+"_"+splitted2(4)+".bmp";
%     
%     im1=imread(directoryimgs+img1);
%     im2=imread(directoryimgs+img2);
%     tform = imregcorr(im1,im2,'rigid');
    
    fid=fopen(directorycores+file1);
    line=fgetl(fid);
    splittedline = split(line);
    xinput = str2double(char(splittedline(3)));
    yinput = str2double(char(splittedline(7)));
    fclose(fid);
    
    fid=fopen(directorycores+file2);
    line=fgetl(fid);
    splittedline = split(line);
    xbase = str2double(char(splittedline(3)));
    ybase = str2double(char(splittedline(7)));
    fclose(fid);
    
    difmat = load(directoryalignmat+file);
    matrot = difmat.value;
    matrot = matrot';
    
    tform = affine2d(difmat.value);
    [xinput,yinput] = transformPointsForward(tform,xinput,yinput);
    
%     inputcord = matrot*[xinput;yinput;1];
%     xinput = inputcord(1);
%     yinput = inputcord(2);
    
    dist = sqrt((xinput-xbase)^2+(yinput-ybase)^2);
    
    fprintf(fileid,"%s,%d\n",char(splitted(1)),dist);
end

fclose(fileid);