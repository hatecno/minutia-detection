clear all;
clc;
close all;

imgc = imread("D:/DEV/Biometria/Scanner OCT/fingerprint/apagar/ImagensTesteQual/Boas/F5LI500.jpg");

blocksize = 20;

%padding
addx=mod(size(imgc,2),blocksize);
addx=blocksize-addx;
addx=size(imgc,2)+addx;

addy=mod(size(imgc,1),blocksize);
addy=blocksize-addy;
addy=size(imgc,1)+addy;

imgc(addy,addx)=0;
%padding

imshow(imgc);

maximos(91) = 0;
matangmaximos(size(imgc,1)/blocksize,size(imgc,2)/blocksize)=0;

for y=0:(size(imgc,1)/blocksize)-1
    for x=0:(size(imgc,2)/blocksize)-1
        block = imgc(y*blocksize+1:y*blocksize+blocksize,x*blocksize+1:x*blocksize+blocksize);

        for ang=0:90
            block_rotated = imrotate(block,ang);
            imgf = fft2(block_rotated);
            %[x,y] = meshgrid(offset:offset+size(block_rotated,2)-1,offset:offset+size(block_rotated,1)-1);
            %figure;
            %surf(x,y,abs(imgf));
            maximos(ang+1) = max(abs(imgf(:))); %salva o valor da fft do angulo (ex angulo 21) na posi��o 22
        end
        
        aux = find(maximos==max(maximos));
        
        matangmaximos(y+1,x+1) = aux(1,1) - 46;
        
        block_rotated = imrotate(block,matangmaximos(y+1,x+1));

        for yy=1:size(block_rotated,1)
            for xx=1:size(block_rotated,2)
                imgc(y*blocksize+yy,x*blocksize+xx) = block_rotated(yy,xx);
            end
        end
    end
end

figure;
imshow(imgc);

% block_rotated = imrotate(block,find(maximos==max(maximos)));
% for eai=offset:offset+size(block_rotated,2)-1
%     for eaj=offset:offset+size(block_rotated,1)-1
%         imgc(eai,eaj)=imgc(eai,eaj)-block_rotated(eai-offset+1,eaj-offset+1);
%     end
% end

% imgf = fft2(block);
% 
% [x,y] = meshgrid(offset:offset+blocksize,offset:offset+blocksize);
% 
% figure;
% %surf(x,y,abs(imgf)/max(abs(imgf(:))));
% surf(x,y,abs(imgf));
