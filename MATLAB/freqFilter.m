clear all;
% clearvars -except imgc;
clc;
close all;

% imgc = imread("C:/Users/Fer_M/Documents/Biometria/Scanner OCT/fingerprint/apagar/ImagensTesteQual/Boas/F5LI500.jpg");
imgc = imread("C:/Users/Fer_M/Documents/Biometria/Scanner OCT/fingerprint/apagar/Base/PROBEJPG500_2/U1RT500.tif");

imgc = imadjust(imgc,[0 1], [1 0]);

blocksize = 16;
win_height = blocksize;
win_width = blocksize*2;

%padding
addx=mod(size(imgc,2),blocksize);
addx=blocksize-addx;
addx=size(imgc,2)+addx;

addy=mod(size(imgc,1),blocksize);
addy=blocksize-addy;
addy=size(imgc,1)+addy;

imgc(addy,addx)=0;
%padding

soma(180,blocksize) = 0.0;
block_rotated = 0;
xsignature(1,win_width)=0;
xsignature_fft_results(1,win_width)=0;
line(blocksize,blocksize) = 0;
imorient(size(imgc,1),size(imgc,2)) = 0;
imfreq(size(imgc,1),size(imgc,2)) = 0;
imfilt(size(imgc,1),size(imgc,2)) = 0;
win(win_height*2,win_width) = 0;
win_rotated(win_height,win_width) = 0;

for y=1:(size(imgc,1)/blocksize)-2 %cuidar com os limites por conta da janela, pensar em um modo melhor
    for x=1:(size(imgc,2)/blocksize)-2 %cuidar com os limites por conta da janela, pensar em um modo melhor
        block = imgc(y*blocksize+1:y*blocksize+blocksize,x*blocksize+1:x*blocksize+blocksize);
        
        for ang=1:10:180
            block_rotated = imrotate(block,ang,'bilinear','crop');
        
            for j=1:blocksize
                for i=1:blocksize
                    soma(ang,j) = soma(ang,j) + double(block_rotated(i,j));
                end
            end
            
            soma(ang,:) = abs(fft(soma(ang,:)));
        end

        angle = find(sum(soma')==max(sum(soma')));
        angle = angle(1);
        angle = 90 - angle;
        line(blocksize/2,:) = 255;
        
        line = imrotate(line,angle,'bilinear','crop');
        
        imorient(y*blocksize+1:y*blocksize+blocksize,x*blocksize+1:x*blocksize+blocksize) = uint8(line);
        
        win = imgc(y*blocksize+1:y*blocksize+win_height*2,x*blocksize+1:x*blocksize+win_width);
        win = imrotate(win,90-angle,'bilinear','crop');
        win_rotated = win(win_height/2:(3*win_height/2)-1,1:win_width);
        
        for j=1:win_width
            for i=1:win_height
                xsignature(1,j) = xsignature(1,j) + double(win_rotated(i,j));
            end
        end
    
        [maxvalues,maxind] = findpeaks(xsignature, 1, 'MinPeakDistance', 5, 'MinPeakHeight', 2000);
        peaks = length(maxind);
        maxind = maxind + 1;
        
%         plot(xsignature);
%         hold on;
%         stem(maxind,maxvalues);
%         figure;
%         imshow(win_rotated);
        
        if peaks > 1
            wavelength = (maxind(end) - maxind(1))/(peaks - 1);
        else
            wavelength = 0;
        end
        
%         if wavelength > 5 & wavelength < 15
        if wavelength > 0
            blockfreq = 1/wavelength;
        else
            blockfreq = 0;
        end
        
        imfreq(y*blocksize+1:y*blocksize+blocksize,x*blocksize+1:x*blocksize+blocksize) = blockfreq.*ones(blocksize,blocksize);

        kx = 0.5;
        ky = 0.5;
        
        if blockfreq
            sigmax = 1/blockfreq*kx;
            sigmay = 1/blockfreq*ky;

            sz = round(3*max(sigmax,sigmay));
            [i,j] = meshgrid(-sz:sz);
%             [i,j] = meshgrid(-blocksize/2:blocksize/2-1);
            reffilter = exp(-(i.^2/sigmax^2 + j.^2/sigmay^2)/2).*cos(2*pi*blockfreq*i);
            
            reffilter = imrotate(reffilter,90+angle,'bilinear','crop');
            
            block_new = imgc(y*blocksize-floor(blocksize/4):y*blocksize+blocksize+floor(blocksize/4)-1,x*blocksize-floor(blocksize/4):x*blocksize+blocksize+floor(blocksize/4)-1);

            block_filtred = conv2(block_new,reffilter,'same');
            
            imfilt(y*blocksize+1:y*blocksize+blocksize,x*blocksize+1:x*blocksize+blocksize) = im2uint8(block_filtred(floor(blocksize/4):blocksize+floor(blocksize/4)-1,floor(blocksize/4):blocksize+floor(blocksize/4)-1));
        else
            block_filtred(:,:) = 0;
        end
        
%         imshowpair(block_new,reffilter,'montage');
%         imshowpair(block,block_new,'montage');

%         figure;
%         imshow(block_filtred);

        soma(:,:) = 0;
        blockpadded(:,:) = 0;
        xsignature(1,:)=0;
        line(:,:) = 0;
        close all;
    end
end

imgc = imadjust(imgc,[0 1], [1 0]);
% imshow(imgc);
imshowpair(imgc,imorient);

imfreq = imfreq/(max(max(imfreq)));
imfreq = im2uint8(imfreq);

figure;
imshow(imfreq);
colormap(hsv);
% imshowpair(imgc,imfreq);

% imfilt = imadjust(imfilt,[0 1], [1 0]);
figure;
% imshowpair(imgc,imfilt);
imshow(imfilt);

figure;
im1 = bwmorph(imfilt,'thin',10);
imshow(im1);

% figure;
% axis([0 size(imgc,2)+blocksize 0 size(imgc,1)+blocksize]);
% 
% 
% for y=1:(size(imgc,1)/blocksize)-3 %cuidar com os limites por conta da janela, pensar em um modo melhor
%     for x=1:(size(imgc,2)/blocksize)-3 %cuidar com os limites por conta da janela, pensar em um modo melhor
%         text(x*blocksize,size(imgc,1) - y*blocksize, string(floor(freqs(y*x)*100)/100), 'FontSize', 8);
%     end
% end