clear all;
%close all;

imgnum=4;
file = "C:/Users/Fer_M/Documents/Biometria/Scanner_OCT/fingerprint/apagar/Base/testegallery/Filtradas/"+imgnum+".dir";
blocksize = 24;

i=1;
fid = fopen(file);
tline=fgetl(fid);
while ischar(tline)
    lines(i) = string(tline);
    linesplitted = split(lines(i));
    x(i) = double(linesplitted(1));
    y(i) = double(linesplitted(3));
    ang(i) = double(linesplitted(5));
    tline=fgetl(fid);
    i=i+1;
end
fclose(fid);

sizex = size(unique(x),2);
sizey = size(unique(y),2);
angles(sizex,sizey) = 0;
ii=0;
jj=1;

for i=1:sizex
    for j=1:sizey-1
        %fprintf("%d, %d | ",x(ii+jj),y(ii+jj));
        angles(i,j) = ang(ii+jj);
        jj=jj+1;
    end
    ii=ii+1;
    %fprintf("\n");
end

centerx = blocksize*size(angles,2)/2;
centery = blocksize*size(angles,1)/2;

poslx = 0;
posly = 0;
posrx = 0;
posry = 0;
posdx = 0;
posdy = 0;
poscorex = 0;
poscorey = 0;
ii = 1;

for i=1:size(angles,1)
    for j=1:size(angles,2)-3
        angle = angles(i,j);
        next = angles(i,j+1);
        nextnext = angles(i,j+2);
        if ( (angle < 90 & next == 90 & nextnext > 90) | (angle < 90 & next > 90) )
            poscorex(ii) = j;
            poscorey(ii) = i;
            ii=ii+1;
        end
    end
end

poscorexuniq = unique(poscorex);

for i=1:size(poscorexuniq,2)
    aux = find(poscorex==poscorexuniq(i));
    if size(aux,2) < 2
        poscorex(aux) = [];
        poscorey(aux) = [];
    end
end

poscorexuniq = unique(poscorex);
change = 1;
poscorexun = 0;
poscoreyun = 0;

for i=1:size(poscorex,2)-1
    if (poscorex(i) - poscorex(i+1)) == 0 
        continue;
    elseif abs(poscorex(i) - poscorex(i+1)) > 0 & abs(poscorex(i) - poscorex(i+1)) < 3
        change = change + 1;
    end
    
    if change == 3
        poscorexun = poscorex(i);
        poscoreyun = poscorey(i);
        change = change+1;
    end
end

if (poscorexun == 0 & poscoreyun == 0) | (size(find(poscorex==poscorexun),2) < 3)
    for i=1:size(poscorexuniq,2)
        numdots =  size(find(poscorex==poscorexuniq(i)),2);
        dots = find(poscorex==poscorexuniq(i));
        if numdots > 2
            if mod(numdots,2) == 0
                poscorexun = poscorex(dots(numdots/2));
                poscoreyun = poscorey(dots(numdots/2));
                break;
            else
                poscorexun = poscorex(dots(((numdots-1)/2)+1));
                poscoreyun = poscorey(dots(((numdots-1)/2)+1));    
                break;
            end
        end
    end
end

poscorex = poscorex*blocksize;
poscorey = poscorey*blocksize;
poscorexun = poscorexun*blocksize;
poscoreyun = poscoreyun*blocksize;

for i=1:size(poscorex,2)
    fprintf("posição x do core: %d, posição y do core: %d\n", poscorex(i), poscorey(i));
end

img = imread("C:/Users/Fer_M/Documents/Biometria/Scanner_OCT/fingerprint/apagar/Base/testegallery/Filtradas/imenh_"+imgnum+".jpg");
figure, imshow(img);
hold on;
scatter(poscorex,poscorey,'r','fill');
scatter(poscorexun,poscoreyun,'b','fill');