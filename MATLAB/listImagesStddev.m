dir1 = "C:\Users\Fer_M\Documents\Biometria\Scanner_OCT\fingerprint\apagar\Base\ARCOSNOVOS\resized\Filtradas\";
allfiles = dir(dir1);

output = dir1+"liststddev.txt";
file_id = fopen(output,'w');

for files=1:size(allfiles,1)
    if allfiles(files).isdir
        continue;
    else
        splitted = split(char(allfiles(files).name),".");
        if splitted(2) ~= "dir"
            continue;
        end
    end
    
    file = dir1+char(allfiles(files).name);
    
    i=1;
    fid = fopen(file);
    tline=fgetl(fid);
    while ischar(tline)
        lines(i) = string(tline);
        linesplitted = split(lines(i));
        if linesplitted(1) == "Core"
            poincare = double(linesplitted(3));
        elseif linesplitted(1) == "stddev"
            stddevfullimg = double(linesplitted(3));
        else
            x(i) = double(linesplitted(1));
            y(i) = double(linesplitted(3));
            ang(i) = double(linesplitted(5));
        end
        tline=fgetl(fid);
        i=i+1;
    end
    fclose(fid);
    
    if stddevfullimg > 1.8
        fprintf(file_id,"%s\n",char(allfiles(files).name));
    end
end

fclose(file_id);