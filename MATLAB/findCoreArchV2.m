clear all;
%close all;

imgnum=5;
file = "C:/Users/Fer_M/Documents/Biometria/Scanner_OCT/fingerprint/apagar/Base/testegallery/Filtradas/"+imgnum+".dir";
blocksize = 24;

i=1;
fid = fopen(file);
tline=fgetl(fid);
while ischar(tline)
    lines(i) = string(tline);
    linesplitted = split(lines(i));
    x(i) = double(linesplitted(1));
    y(i) = double(linesplitted(3));
    ang(i) = double(linesplitted(5));
    tline=fgetl(fid);
    i=i+1;
end
fclose(fid);

sizex = size(unique(x),2);
sizey = size(unique(y),2);
angles(sizex,sizey) = 0;
ii=0;
jj=1;

for i=1:sizex
    for j=1:sizey-1
        %fprintf("%d, %d | ",x(ii+jj),y(ii+jj));
        angles(i,j) = ang(ii+jj);
        jj=jj+1;
    end
    ii=ii+1;
    %fprintf("\n");
end

centerx = blocksize*size(angles,2)/2;
centery = blocksize*size(angles,1)/2;

poslx = 0;
posly = 0;
posrx = 0;
posry = 0;
posdx = 0;
posdy = 0;

for i=2:size(angles,1)-1
    for j=2:size(angles,2)-1
        angle1 = angles(i,j);
        angle2 = angles(i-1,j+1);
        angle3 = angles(i+1,j-1);
        angleavg = (angle1+angle2+angle3)/3;
        dist = sqrt((centery-i*blocksize)^2+(centerx-j*blocksize)^2);
%         if abs(angleavg-angle1) > 10 | abs(angleavg-angle2) > 10 | abs(angleavg-angle3) > 10
%             continue;
%         end
        if angle1 > 30 & angle1 < 50 & dist < 100
            poslx = j;
            posly = i;
        end
        angle2 = angles(i-1,j-1);
        angle3 = angles(i+1,j+1);
        angleavg = (angle1+angle2+angle3)/3;
%         if abs(angleavg-angle1) > 10 | abs(angleavg-angle2) > 10 | abs(angleavg-angle3) > 10
%             continue;
%         end
        if angle1 > 110 & angle1 < 140 & dist < 120
            posrx = j;
            posry = i;
        end
        angle2 = angles(i,j-1);
        angle3 = angles(i,j+1);
        angleavg = (angle1+angle2+angle3)/3;
%         if abs(angleavg-angle1) > 10 | abs(angleavg-angle2) > 10 | abs(angleavg-angle3) > 10
%             continue;
%         end
        if angle1 > 80 & angle1 < 110 & dist < 100
            posdx = j;
            posdy = i;
        end
    end
end

poscorex = (poslx+posrx+posdx)/3;
poscorey = (posly+posry+posdy)/3;

poscorex = poscorex*blocksize;
poscorey = poscorey*blocksize;

fprintf("posição x do core: %d, posição y do core: %d\n",poscorex, poscorey);

img = imread("C:/Users/Fer_M/Documents/Biometria/Scanner_OCT/fingerprint/apagar/Base/testegallery/Filtradas/imenh_"+imgnum+".jpg");
figure, imshow(img);
hold on;
r = 10;
th = 0:pi/50:2*pi;
xunit = r * cos(th) + poscorex;
yunit = r * sin(th) + poscorey;
h = plot(xunit, yunit,'r');