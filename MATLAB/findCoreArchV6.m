directory = "C:\Users\Fer_M\Documents\Biometria\Scanner_OCT\fingerprint\apagar\Base\ARCOSNOVOS\resized\";
% directory = "C:\Users\Fer_M\Documents\Biometria\Scanner_OCT\fingerprint\apagar\Base\testegallery\";
imgsdir = directory+"Filtradas\";
allfiles = dir(imgsdir);

for imgs=1:size(allfiles,1)
    if allfiles(imgs).isdir
        continue;
    else
        splitted = split(char(allfiles(imgs).name),".");
        if splitted(2) ~= "dir"
            continue;
        end
    end
    
    file = imgsdir+char(allfiles(imgs).name);
    blocksize = 24;

    i=1;
    fid = fopen(file);
    tline=fgetl(fid);
    while ischar(tline)
        lines(i) = string(tline);
        linesplitted = split(lines(i));
        if linesplitted(1) == "Core"
            poincare = double(linesplitted(3));
        elseif linesplitted(1) == "stddev"
            stddevfullimg = double(linesplitted(3));
        else
            x(i) = double(linesplitted(1));
            y(i) = double(linesplitted(3));
            ang(i) = double(linesplitted(5));
        end
        tline=fgetl(fid);
        i=i+1;
    end
    fclose(fid);

    sizex = size(unique(x),2);
    sizey = size(unique(y),2);
    angles(sizex,sizey) = 0;
    ii=0;
    jj=1;

    for i=1:sizex
        for j=1:sizey-1
            %fprintf("%d, %d | ",x(ii+jj),y(ii+jj));
            angles(i,j) = ang(ii+jj);
            jj=jj+1;
        end
        ii=ii+1;
        %fprintf("\n");
    end

    centerx = blocksize*size(angles,2)/2;
    centery = blocksize*size(angles,1)/2;

    poslx = 0;
    posly = 0;
    posrx = 0;
    posry = 0;
    posdx = 0;
    posdy = 0;
    poscorex = 0;
    poscorey = 0;
    ii = 1;

    for i=1:size(angles,1)
        for j=1:size(angles,2)-3
            angle = angles(i,j);
            next = angles(i,j+1);
            nextnext = angles(i,j+2);
            if (angle < 90 & next > 90)
                poscorex(ii) = j;
                poscorey(ii) = i;
                ii=ii+1;
            elseif (angle < 90 & nextnext > 90)
                poscorex(ii) = j;
                poscorey(ii) = i;
                ii=ii+1;
            end
        end
    end

    poscorexuniq = unique(poscorex);

    for i=1:size(poscorexuniq,2)
        aux = find(poscorex==poscorexuniq(i));
        if size(aux,2) < 2
            poscorex(aux) = [];
            poscorey(aux) = [];
        end
    end

    poscorexuniq = unique(poscorex);
    change = 1;
    poscorexun = 0;
    poscoreyun = 0;

    for i=1:size(poscorexuniq,2)
        numdots(i) =  size(find(poscorex==poscorexuniq(i)),2);
    end

    [maxnumdotsrow,xindmax] = max(numdots);
    [dots,pos]  = find(poscorex==poscorexuniq(xindmax));
    ylimit = poscorey(pos);
    
    if size(ylimit,2) > 7
        newylimit = ylimit(4:size(ylimit,2)-3);
    elseif size(ylimit,2) > 5
        newylimit = ylimit(3:size(ylimit,2)-2);
    elseif size(ylimit,2) > 3
        newylimit = ylimit(2:size(ylimit,2)-1);
    else
        newylimit = ylimit;
    end
    
    ylimitmax = max(newylimit);
    ylimitmin = min(newylimit);

    for i=ylimitmin:ylimitmax
        angle = angles(i,poscorexuniq(xindmax));
        next = angles(i,poscorexuniq(xindmax)+1);
        nextnext = angles(i,poscorexuniq(xindmax)+2);
        if abs(next-angle) > abs(nextnext-angle)
            dif(i) = abs(next-angle);
        else
            dif(i) = abs(nextnext-angle);
        end
    end
    
    difmean = mean(dif);
    
    if poincare
        poscorex = poscorexuniq(xindmax)*blocksize;
        poscorey = poincare;
    else
        if difmean > 60
            [maxdif,yindmax] = max(dif);
            poscorex = poscorexuniq(xindmax)*blocksize;
            poscorey = yindmax*blocksize;
        else
            poscorex = poscorexuniq(xindmax)*blocksize;
            poscorey = round((ylimitmax+ylimitmin)/2)*blocksize;
        end
    end

%     poscorex = poscorex*blocksize;
%     poscorey = poscorey*blocksize;
    poscorexun = poscorexun*blocksize;
    poscoreyun = poscoreyun*blocksize;

    for i=1:size(poscorex,2)
        fprintf("posição x do core: %d, posição y do core: %d\n", poscorex(i), poscorey(i));
    end

    file=directory+"Cores\"+splitted(1)+".txt";
    fid=fopen(file,'w');
    fprintf(fid,"x : %d , y : %d",poscorex,poscorey);
    fclose(fid);
    
    img = imread(directory+splitted(1)+".bmp");
%     imagesave = figure;
%     imshow(img);
%     hold on;
%     scatter(poscorex,poscorey,'r','fill');
%     scatter(poscorex,poincare,'b','fill');
    img_out = insertShape(img, 'circle', [poscorex poscorey 6],'LineWidth',5,'Color','red');
    imwrite(img_out,directory+"Cores\"+splitted(1)+".bmp");
    clearvars -except allfiles imgsdir directory
    close all;
end