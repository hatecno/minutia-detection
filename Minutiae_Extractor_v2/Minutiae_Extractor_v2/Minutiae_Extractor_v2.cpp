// Minutiae_Extractor.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "headerminutia.h"

int main(int argc, char** argv)
{
    //string imagesDir("D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseAtual/Gallery/");
    //string imagesDir("D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseAtual/Probe/");
    //string imagesDir("D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseDerme/cropGallery/");
    //string imagesDir("D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseDerme/cropProbe/");

    string imagesDir("D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseAtual/Probe/");

    vector<string> imagesNames;

    vector<string> filesclassification;
    vector<int> classifications;

    vector<minutiae> extractedminutiaes;

    if (argc > 1)
    {
        imagesDir = argv[1];
        imagesNames.clear();
        for (int i = 2; i < argc; ++i)
        {
            imagesNames.push_back(argv[i]);
        }
    }

    if (imagesNames.empty())
    {
        for (std::filesystem::directory_entry entry : std::filesystem::directory_iterator(imagesDir)) {
            if (!entry.is_directory()) imagesNames.push_back(entry.path().filename().string());
        }
    }

    for (int figure = 0; figure < imagesNames.size(); ++figure) {

        Mat image = imread(imagesDir + imagesNames[figure], IMREAD_GRAYSCALE); // Read the file

        if (image.empty())  // Check for invalid input
        {
            cout << "Nao foi possivel abrir ou encontrar a impressao digital: " << imagesNames[figure] << std::endl;
            continue;
        }

        verifyInputImageDefaultSize(image, image);

        Mat minmap;
        float meanwavelen = 8, meandifang = 6;
        int blocksize = 20, fpclassification = 1;

        cout << "Validando os requisitos para extracao das minucias da impressao digital: " << imagesNames[figure] << "\n";

        Mat imthin = imread(imagesDir + "Filtradas/imthin_" + imagesNames[figure], IMREAD_GRAYSCALE); // Read the file

        if (imthin.empty())  // Check for invalid input
        {
            cout << "Nao foi possivel abrir ou encontrar a imagem esqueletonizada de: " << imagesNames[figure] << std::endl;
            continue;
        }

        Mat imorient = imread(imagesDir + "Filtradas/imorient_" + imagesNames[figure], IMREAD_GRAYSCALE); // Read the file

        if (imorient.empty())  // Check for invalid input
        {
            cout << "Nao foi possivel abrir ou encontrar a imagem do mapa direcional de: " << imagesNames[figure] << std::endl;
            continue;
        }

        Mat imsegmented = imread(imagesDir + "Filtradas/imsegmented_" + imagesNames[figure], IMREAD_GRAYSCALE); // Read the file

        if (imsegmented.empty())  // Check for invalid input
        {
            cout << "Nao foi possivel abrir ou encontrar a imagem segmentada de: " << imagesNames[figure] << std::endl;
            continue;
        }

        Mat imwave = imread(imagesDir + "Filtradas/imwave_" + imagesNames[figure], IMREAD_GRAYSCALE); // Read the file

        if (imwave.empty())  // Check for invalid input
        {
            cout << "Nao foi possivel abrir ou encontrar a imagem de frequencias de: " << imagesNames[figure] << std::endl;
            continue;
        }

        cout << "Extraindo as minucias da impressao digital: " << imagesNames[figure] << "\n";
        cout << "Processando...\n";

        meanwavelen = meanWavelengthNormalizedImage(imwave, blocksize);
        meandifang = meanDifAngleDirMap(imorient, blocksize);

        extractedminutiaes = findMinutiae(image, imthin, imorient, imwave, imsegmented, minmap, imagesDir, imagesNames[figure], 0, blocksize, meanwavelen, meandifang, fpclassification);

        extractedminutiaes = penalizeSpuriousMinutiaes(extractedminutiaes);

        writeMinFile(extractedminutiaes, imagesDir, imagesNames[figure]);
        imwrite(imagesDir + "Filtradas/min_" + imagesNames[figure], minmap);
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
