// QualityMap.cpp : Este arquivo contém a função 'main'. A execução do programa começa e termina ali.
//

#include "QualityMapHeader.h"

int main()
{
    std::vector<minutiae> mins;
    std::string dir = "D:/apagar/Biometria/Scanner_OCT/fingerprint/apagar/Base/BaseDerme/imgs_novas_epiderme/Probe/Filtradas/";
    //std::string file = "U2RI_0 -epiderme_sum";
    cv::Mat imagein, imageout, stddevimage;
    float stddevfullimg = 0;
    std::vector<std::string> filenames;

    for (std::filesystem::directory_entry entry : std::filesystem::directory_iterator(dir)) {
        if (!entry.is_directory() && (entry.path().extension().compare(".min") == 0)) filenames.push_back(entry.path().filename().string());
    }

    for (int files = 0; files < filenames.size();++files) {
        mins = readMinFile(dir + filenames[files]);
        imagein = cv::imread(dir + "../" + removeExtension(filenames[files]) + ".jpg", cv::IMREAD_GRAYSCALE);

        cv::resize(imagein, imagein, cv::Size(400, 400));

        createQualityMap(mins, imagein, imageout);

        imwrite(dir + "qual_" + removeExtension(filenames[files]) + ".jpg", imageout);
    }

    return 0;
}

// Executar programa: Ctrl + F5 ou Menu Depurar > Iniciar Sem Depuração
// Depurar programa: F5 ou menu Depurar > Iniciar Depuração

// Dicas para Começar: 
//   1. Use a janela do Gerenciador de Soluções para adicionar/gerenciar arquivos
//   2. Use a janela do Team Explorer para conectar-se ao controle do código-fonte
//   3. Use a janela de Saída para ver mensagens de saída do build e outras mensagens
//   4. Use a janela Lista de Erros para exibir erros
//   5. Ir Para o Projeto > Adicionar Novo Item para criar novos arquivos de código, ou Projeto > Adicionar Item Existente para adicionar arquivos de código existentes ao projeto
//   6. No futuro, para abrir este projeto novamente, vá para Arquivo > Abrir > Projeto e selecione o arquivo. sln
