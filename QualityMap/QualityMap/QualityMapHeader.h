#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <filesystem>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;

struct minutiae {
    int id, x, y, angle, type;
    float wavelength, quality, radius, theta, vertangle;
};

vector<minutiae> readMinFile(string file);
void createQualityMap(std::vector<minutiae> mins, cv::InputArray imagein, cv::OutputArray imageout);
void stdDevImage(cv::InputArray image, cv::OutputArray mask, int blocksize, float& stddevfullimg);
std::string removeExtension(std::string filename);

std::string removeExtension(std::string filename)
{
    size_t pos = 0;
    std::string delimiter = ".";

    pos = filename.find(delimiter);
    if (pos != std::string::npos) {
        filename.erase(pos, filename.length());
    }

    return filename;
}

vector<minutiae> readMinFile(string file) {
    int indexcaract = 0, mincont = 0;
    size_t pos = 0, posxy = 0;
    string line;
    string token;
    string delimiter = ":";
    ifstream infile(file);
    vector<minutiae> minutiaes;
    string::iterator auxtoken;

    while (getline(infile, line)) {
        //cout << line << endl;

        minutiaes.push_back(minutiae());
        indexcaract = 0;
        while ((pos = line.find(delimiter)) != string::npos) {
            token = line.substr(0, pos);
            token.erase(remove_if(token.begin(), token.end(), [](unsigned char x) {return isspace(x); }), token.end());
            //cout << token << endl;

            switch (indexcaract)
            {
            case 0:
                minutiaes[mincont].id = stoi(token);
                break;
            case 1:
                posxy = token.find(",");
                minutiaes[mincont].x = stoi(token.substr(0, posxy));
                token.erase(0, posxy + 1);
                minutiaes[mincont].y = stoi(token);

                minutiaes[mincont].radius = sqrt(pow(minutiaes[mincont].x, 2) + pow(minutiaes[mincont].y, 2));
                minutiaes[mincont].theta = atan(minutiaes[mincont].y / (minutiaes[mincont].x + FLT_EPSILON));
                break;
            case 2:
                minutiaes[mincont].angle = stoi(token);
                break;
            case 3:
                minutiaes[mincont].quality = stof(token);
                break;
            case 4:
                if (token.compare("RIG") == 0) {
                    minutiaes[mincont].type = 0;
                }
                else if (token.compare("BIF") == 0) {
                    minutiaes[mincont].type = 1;
                }
                else {
                    minutiaes[mincont].type = 2;
                }
                break;
            case 5:
                minutiaes[mincont].wavelength = stof(token);
                break;
            default:
                break;
            }

            line.erase(0, pos + delimiter.length());
            ++indexcaract;
        }

        line.erase(remove_if(line.begin(), line.end(), [](unsigned char x) {return isspace(x); }), line.end());

        minutiaes[mincont].vertangle = stof(line);
        //cout << line << endl;

        ++mincont;
    }

    return minutiaes;
}

void createQualityMap(std::vector<minutiae> mins, cv::InputArray imagein, cv::OutputArray imageout)
{
    cv::Mat image, stddevimage;
    cv::Mat3b qualityMap, imageinBGR;

    int blocksize = 30;
    int blockx = 0, blocky = 0;
    float stddevfullimg = 0;

    imagein.copyTo(image);

    stdDevImage(image, stddevimage, 20, stddevfullimg);

    cvtColor(image, imageinBGR, cv::COLOR_GRAY2BGR);

    imageinBGR *= 0.8f;
    
    qualityMap = cv::Mat::zeros(image.rows, image.cols, CV_8UC3);

    for (int i = 0; i < stddevimage.rows;++i) {
        for (int j = 0; j < stddevimage.cols; ++j) {
            qualityMap.at<cv::Vec3b>(i, j)[0] = (int)round(stddevimage.at<float>(i, j));
        }
    }

    for (int i = 0; i < mins.size(); ++i) {
        if (mins[i].quality > 0.6f) {
            //green
            blockx = mins[i].x - (blocksize / 2);
            blocky = mins[i].y - (blocksize / 2);

            if (blockx < 0) blockx = 0;
            if (blocky < 0) blocky = 0;
            if ((blockx + blocksize) > image.cols) blockx = image.cols - blocksize;
            if ((blocky + blocksize) > image.rows) blocky = image.rows - blocksize;

            for (int k = blocky; k < blocky + blocksize; ++k) {
                for (int j = blockx; j < blockx + blocksize; ++j) {
                    qualityMap.at<cv::Vec3b>(k, j)[0] = 0;
                    qualityMap.at<cv::Vec3b>(k, j)[1] = 100;
                    qualityMap.at<cv::Vec3b>(k, j)[2] = 0;
                }
            }
        }
        else if (mins[i].quality > 0.3f) {
            //yellow
            blockx = mins[i].x - (blocksize / 2);
            blocky = mins[i].y - (blocksize / 2);

            if (blockx < 0) blockx = 0;
            if (blocky < 0) blocky = 0;
            if ((blockx + blocksize) > image.cols) blockx = image.cols - blocksize;
            if ((blocky + blocksize) > image.rows) blocky = image.rows - blocksize;

            for (int k = blocky; k < blocky + blocksize; ++k) {
                for (int j = blockx; j < blockx + blocksize; ++j) {
                    qualityMap.at<cv::Vec3b>(k, j)[0] = 0;
                    qualityMap.at<cv::Vec3b>(k, j)[1] = 100;
                    qualityMap.at<cv::Vec3b>(k, j)[2] = 100;
                }
            }
        }
        else {
            //red
            blockx = mins[i].x - (blocksize / 2);
            blocky = mins[i].y - (blocksize / 2);

            if (blockx < 0) blockx = 0;
            if (blocky < 0) blocky = 0;
            if ((blockx + blocksize) > image.cols) blockx = image.cols - blocksize;
            if ((blocky + blocksize) > image.rows) blocky = image.rows - blocksize;

            for (int k = blocky; k < blocky + blocksize; ++k) {
                for (int j = blockx; j < blockx + blocksize; ++j) {
                    qualityMap.at<cv::Vec3b>(k, j)[0] = 0;
                    qualityMap.at<cv::Vec3b>(k, j)[1] = 0;
                    qualityMap.at<cv::Vec3b>(k, j)[2] = 100;
                }
            }
        }
    }

    imageinBGR += qualityMap;

    imageinBGR.copyTo(imageout);

    return;
}

void stdDevImage(cv::InputArray image, cv::OutputArray mask, int blocksize, float& stddevfullimg)
{
    cv::Mat stddevim, stddevim_temp;
    cv::Mat aux = cv::Mat::zeros(cv::Size(blocksize, blocksize), CV_32FC1);
    cv::Scalar mean, stddev;
    float stddev_fullimg;
    int addy, addx;

    image.copyTo(stddevim);
    stddevim.convertTo(stddevim, CV_32FC1);

    //pad image to blocksize
    addy = stddevim.rows % blocksize;
    if (addy != 0) addy = blocksize - addy;

    addx = stddevim.cols % blocksize;
    if (addx != 0) addx = blocksize - addx;

    copyMakeBorder(stddevim, stddevim, 0, addy, 0, addx, cv::BORDER_CONSTANT, cv::Scalar(255));

    stddev_fullimg = 0;

    for (int y = 0; y < stddevim.rows; y += blocksize) {
        for (int x = 0; x < stddevim.cols; x += blocksize) {
            aux = stddevim(cv::Rect(x, y, blocksize, blocksize));

            cv::meanStdDev(aux, mean, stddev); //calculate stddev to populate / desired value in mean[0] and stddev[0]

            for (int i = 0; i < blocksize; ++i) {
                for (int j = 0; j < blocksize; ++j) {
                    stddevim.at<float>(y + i, x + j) = stddev[0];
                }
            }
        }
    }

    for (int y = 0; y < stddevim.rows; y += blocksize) {
        for (int x = 0; x < stddevim.cols; x += blocksize) {
            stddev_fullimg = stddev_fullimg + (stddevim.at<float>(y, x) / 255.0f);
        }
    }

    stddev_fullimg = stddev_fullimg / ((stddevim.rows / blocksize) + (stddevim.cols / blocksize));

    stddevfullimg = stddev_fullimg;

    stddevim_temp = stddevim(cv::Rect(0, 0, stddevim.cols - addx, stddevim.rows - addy));

    stddevim_temp.copyTo(mask);
}

